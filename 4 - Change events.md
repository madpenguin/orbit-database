# Change Events - real time monitoring

#### Change events are a key feature which enable the UI to reflect database changes in real-time. The design assumes that you will have a central connection for UI / web clients which will broadcast any changes. So any process can make a change to a database row, but in order to receive a notification of the change, you need to be connected to the process that's actually doing the monitoring.

### How does it work?

To try to explain 'what it is', assuming auditing is enable on the database (auditing=True when you open the database) then a watcher thread will be created which which runs a background asyncio event loop. For each table opened with (auditing=True) , the background process will capture any updates to the table and build an event log.

For each table, you can then assign zero or more callback routines which are activated asynchronously with details of the changes made on a row-by-row basis. The async facility mean that the callback function can efficiently call other async code (such as async networking code) to feed those changes back to where they are needed. (for example a web UI)

<table><tr><td>
<img style="background-color: white; height:400px;padding: 2em;border-radius:6px" src="images/orbit-auditing.svg" /><td>
<td><b style="padding-left: 1.4em">Key Points<b>

 * Any process can read/write the database
 * Call handlers live in the main process
 * Call handler run in a separate thread
 * Call handlers are async (not sync!)
 * Main process needs to run an event loop
 * All processes must have the UID of the db
 * Any number of clients could be connected
 * Auditing is on/off at database level
 * Individual tables can then audit or not
</td></tr></table>

## How do we do it?

Now for a practical example, consider the following block of code which does the following;
* Add is going to add a record to the people table
* It does this by opening the database with auditing turned on, opens the table with auditing turned on, and assigns "callback" to be the routine called when the table is updated.
* We then wait (asynchronously) for a second, this gives the auditing thread chance to run

```python
#!/usr/bin/env python
from orbit_database import Manager, Doc
import asyncio

async def callback (docs):
    print(f'<< callback >>')
    for doc in docs:
        print(doc.doc)

async def add ():
    db = Manager().database('dbdemo', config={'auditing': True})
    people = db.table('people', auditing=True, callback=callback)
    people.append(Doc({'name': 'Auditor', 'age': 'infinite'}))
    await asyncio.sleep(1)

loop = asyncio.new_event_loop()
loop.run_until_complete(add())
loop.close()
```

If we run this cose, the only output comes from the callback routine, which has run as an asynchronous task within a background thread;

```bash
$ ./db.py 
<< callback >>
{'t': 'people', 'o': '41d9174c12a5eae5005732a75779', 'e': 1, 'c': {'name': 'Auditor', 'age': 'infinite'}}
```

In this instance the field passed represents a create action;

* t: the table we're adding to
* o: the primary key we're adding
* e: 1 = create
* c: the record being added

### Updating records

If we modify this code to update a record as follows;

```python
async def update ():
    db = Manager().database('dbdemo', config={'auditing': True})
    people = db.table('people', auditing=True, callback=callback)
    person = people.first()
    person._age = 'finite'
    people.save(person)
    await asyncio.sleep(1)
```

```bash
$ ./db.py 
<< callback >>
{'t': 'people', 'o': '41d916b1962757f9005732a78dc5', 'e': 2, 'c': {'name': 'Tom', 'age': 20}}
```

So now we have a slightly different update;

* t: the table we are amending
* o: the primary key of the record we are amending
* e: 2 = amend
* c: the previous version of the record

Note that 'c' is the record 'before' the update is applied, the new version is obviously available "in" the database. So this gives any monitoring application the opportunity to see the what has changed.

##### <b>Note</b>; the version in the database will always be the current version, but the version that appears in an audit version will be 'a' previous version. If you are modifying the same record multiple times in quick succession, you can't rely on it always being "the" previous version.

### Deleting records

The last thing we need to account for is deletions, if we change the code again;

```python
async def update ():
    db = Manager().database('dbdemo', config={'auditing': True})
    people = db.table('people', auditing=True, callback=callback)
    person = people.first()
    people.delete(person)
    await asyncio.sleep(1)
```

##### <b>Note</b>; the delete method can take a number of different parameters to specify the item to delete. You can either supply a primary key, list of primary keys, or an actual 'Doc' object.

```bash
$ ./db.py 
<< callback >>
{'t': 'people', 'o': '41d916b1962757f9005732a78dc5', 'e': 3, 'c': {'name': 'Tom', 'age': 'finite'}}
```

So now we have a slightly different update;

* t: the table we are amending
* o: the primary key of the record we are deleting
* e: 3 = delete
* c: the deleted record

The Orbit Framework uses this mechanism to feed database changes directly back into connected websocket based applications. The audit table itself is persistent so technically a crash in the server does not mean clients transactions will be lost. Each audit transaction is written within the same transaction used to update the record in question, so from a technical perspective it's impossible to "lost" events. That said, if a client loses it's connection to the monitoring process, there is no buffering so the client will need to refresh any queries it's using before it starts listening to the change queue.

##### <b>Note</b>; client applications should be written in such a way that they are driven from current state, rather than from change detection. When connecting to a changing data source and listening for updates, an initial sync that relies on differences becomes very difficult to perfect and as a result very fragine. Relying on current state means you can start listening to changes, grab a current set of query results, and ignore any duplication or overlap. This makes synchronisation very easy and very robust as the expense of a tiny amount of traffic duplication on connect!

