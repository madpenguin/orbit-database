# Common actions (the cheatsheet)

#### There are something you may only ever do once and as a result may take a little time to get a handle on. Most of the time you're more likly to be repeating the same requirement or pattern in slightly different contexts, so we'll try do document some of the more common use-cases.

### The Database Structure

Access to the database is designed to be relatively Pythonic or intuitive. As such we can access our database structure from the top-down as a series of linked arrays. That is to say, within the database managed, we have a list of databases, within each database we have a list of tables, and within each table we have a list of indexes. So if you need to work on the database structure, it's easy to iterate through databases, tables and indecies.

```python
from orbit_database import Manager
manager = Manager()
database = manager.database('starwars')
actors = database.table('actors')
print("Open Databases.......> ^S", list(manager))
print("Tables in starwars...> ", list(database))
print("Indexes in actors....> ", list(actors))
...
Open Databases.......>  ['starwars']
Tables in starwars...>  ['@@metadata@@', '@@audit_table@@', 'actors']
Indexes in actors....>  ['by_name', 'by_word']
```
### Getting a record from it's Primary key

All we need to do here is call the table's "get" method with the primary key we want. It will either return a valid "Doc" object of the primary key exists, or it will return "None".

```python
print(actors.get('41d812f1768fe6af005732a799a6'))
print(actors.get('nosuchkey'))
...
<class 'orbit_database.doc.Doc'> with id=b'41d812f1768fe6af005732a799a6'
None
```
### First and last records

There may be instances, at the very least when testing, when we need to get the first and last records in any given table or index. This is literally as easy as it sounds;

```python
print(f'First (primary) ... > {actors.first()._name}')
print(f'Last (primary) .... > {actors.last()._name}')
print(f'First (by_name) ... > {actors.first("by_name")._name}')
print(f'Last (by_name) .... > {actors.last("by_name")._name}')
...
First (primary) ... > Luke Skywalker
Last (primary) .... > Baby Yoda
First (by_name) ... > Baby Yoda
Last (by_name) .... > Wilhuff Tarkin
```

### Iterating (and paging) through all records

As mentioned earlier, the 'filter' method is your 'goto' for recovering data from the database. So to get all record by primary key;

```python
for result in actors.filter():
    print(result.doc.key, result.doc._name)
...
41d812f1768fe6af005732a799a6 Luke Skywalker
41d812f1768fe765005732a799a6 Darth Vader
41d812f1768fe7ba005732a799a6 Han Solo
41d812f1768fe804005732a799a6 Leia Organa
41d812f1768fe84b005732a799a6 Wilhuff Tarkin
41d812f1768fe88e005732a799a6 C-3PO
41d812f1768fe8d1005732a799a6 R2-D2
41d813e3027afce9005732a70a9e Baby Yoda
```

Or if we want records using the 'by_name' index, in reverse order, limited to the first 4 records;

```python
for result in actors.filter('by_name', reverse=True, page_size=4):
    print(result.doc.key, result.doc._name)
...
41d812f1768fe84b005732a799a6 Wilhuff Tarkin
41d812f1768fe8d1005732a799a6 R2-D2
41d812f1768fe6af005732a799a6 Luke Skywalker
41d812f1768fe804005732a799a6 Leia Organa
```
If we then wanted the remaining record (or you could just as easily do the next 4 etc) then you could use the context option. Just set the context to be the 'previous' result and it will pick up from where it left off. This uses indexes if they are available so it' very efficient if you're implementing any kind of paging system where you might need to cycle through blocks of records. As the last result is just a record, if you need to you can easily search a table to locate a start point, then 'fabricate' a context to start paging from any given point.

```python
print('More ...')
for result in actors.filter('by_name', reverse=True, context=result):
    print(result.doc.key, result.doc._name)
...
More ...
41d812f1768fe7ba005732a799a6 Han Solo
41d812f1768fe765005732a799a6 Darth Vader
41d812f1768fe88e005732a799a6 C-3PO
41d813e3027afce9005732a70a9e Baby Yoda
```

### Searching for data using indecies

This is a slightly more complex task, but literally all you need to do is set a lower and upper bound and the indexing will do everything else for you. So to find all names for example begging with 'L'; (note that what you use as an upper bound will depend on your key values, but '|' works well for text indecies as it's greater than 'z')

```python
lower = Doc({'name': 'L'})
upper = Doc({'name': 'L|'})
for result in actors.filter('by_name', lower=lower, upper=upper):
    print(result.doc.key, result.doc._name)
...
41d812f1768fe804005732a799a6 Leia Organa
41d812f1768fe6af005732a799a6 Luke Skywalker
```

### Taking advantage of duplicate indecies

Duplicate indecies can be very handy, not only from the perspective of simply being able to index multiple documents under the same key, but also the free unique count that comes with it. Recovering the number of documents matching a given search range is simply an index read and doesn't even require a data lookup. If your application uses something like "tags" for example where you might add zero or more tags to each record, acquiring a unique list of tags and the number of records each tags to attached to, is almost a 'free' operation in terms of resources used.

In our example dataset we have a "primary function" field, which can have duplicate values;

```bash
starwars> select actors name, primary_function
┏━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ name                  ┃ primary_function       ┃
┡━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━┩
│ Luke Skywalker        │ Human                  │
│ Darth Vader           │ Human                  │
│ Han Solo              │ Human                  │
│ Leia Organa           │ Human                  │
│ Wilhuff Tarkin        │ null                   │
│ C-3PO                 │ Protocol               │
│ R2-D2                 │ Astromech              │
│ Baby Yoda             │ null                   │
└───────────────────────┴────────────────────────┘
           Time: 0.0000s => 190472/sec      
```

So if we add a duplicate index based on primary function, we can effectively group records by primary function. Note that records with no value for the specified index key, are not indexes. In this instance, two of the records hand a null value for primary_function hence do not appear in the index.

```python
actors.ensure('by_fn', '{primary_function}', duplicates=True)
for result in actors.filter('by_fn', suppress_duplicates=True):
    print(result.doc.key, result.count, result.doc._primary_function)
...
41d812f1768fe8d1005732a799a6 1 Astromech
41d812f1768fe6af005732a799a6 4 Human
41d812f1768fe88e005732a799a6 1 Protocol
```
```bash
starwars> unique actors primary_function index=by_fn
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━┓
┃ Field name                   ┃ Count           ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━┩
│ Astromech                    │ 1               │
│ Human                        │ 4               │
│ Protocol                     │ 1               │
└──────────────────────────────┴─────────────────┘
            Time: 0.0002s => 15306/sec
```

### Counting the records in a table or index

Just call the "records()" method on either the table or the index;
```python
print(f'Actors = {actors.records()}')
print(f'Index entries: {actors["by_fn"].records()}')
...
Actors = 8
Index entries: 6
```

### Adding new data

This is done by passing a new "Doc()" object to a table's "append" method. The Doc object has a handy initialiser such that all we need to do it to pass it a dictionary and it will do all the rest, so;

```python
actors.append(Doc({
        'name': 'Chewbaccar',
        'home_planet': 'Kashyyyk',
        'type': 2,
        'primary_function': 'Wookie'
    }))
```

To update the data it's just as easy. Locate the record, change the field(s) you want to change, then call 'save'. So in this instance we spelt the name wrong (deliberate mistake!) so to fix it;

```python
actor = actors.seek_one('by_name', Doc({'name': 'Chewbaccar'}))
actor._name = 'Chewbacca'
actors.save(actor)
```
```bash
starwars> select actors name, type, primary_function
┏━━━━━━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━━━━━━┓
┃ name              ┃ type  ┃ primary_function   ┃
┡━━━━━━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━━━━━━┩
│ Luke Skywalker    │ 1     │ Human              │
│ Darth Vader       │ 1     │ Human              │
│ Han Solo          │ 1     │ Human              │
│ Leia Organa       │ 1     │ Human              │
│ Wilhuff Tarkin    │ 1     │ null               │
│ C-3PO             │ 2     │ Protocol           │
│ R2-D2             │ 2     │ Astromech          │
│ Baby Yoda         │ null  │ null               │
│ Chewbacca         │ 2     │ Wookie             │
└───────────────────┴───────┴────────────────────┘
           Time: 0.0001s => 173070/sec       
```
In this instance we've take a short-cut, seek_one works in much the same way as filter, but it's designed to get a single (unique) record from the database. Note that seek_one isn't useful on fields or indecies that might contain duplicates (!)

##### Note: you may see things written slightly differently when using the Orbit Framework in conjunction with Orbit Database. The framework provides an ORM by default which adds very little overhead yet makes the semantics of reading / writing more flexible. Because the ORM is data centric you can do things like "actors.seek('Chewbaccar').update({'name': 'Chewbacca'}).save()", which at the very least is a little more concise.

