# The Database Shell

#### The database shell lives and is documented in a different repository however it is a useful tool for working with Orbit Database so we'll cover a brief introduction here. Bear in mind detail may changhe a little and you should head over to the orbit-database-shell repository for up-to-date documentation.

### Installation

Start off by creating a virtual environment using the tool of your choosing (we currently use "pyenv") then (don't forget to activate the environment) do;
```bash
pip install orbit_database_shell
```
And to run the shell just do;

```bash
$ orbit_database_shell 

  .oooooo.             .o8        o8o      .           oooooooooo.   oooooooooo.
 d8P'  `Y8b           "888        `"'    .o8           `888'   `Y8b  `888'   `Y8b
888      888 oooo d8b  888oooo.  oooo  .o888oo          888      888  888     888
888      888 `888""8P  d88' `88b `888    888            888      888  888oooo888'
888      888  888      888   888  888    888   8888888  888      888  888    `88b
`88b    d88'  888      888   888  888    888 .          888     d88'  888    .88P
 `Y8bood8P'  d888b     `Y8bod8P' o888o   "888"         o888bood8P'   o888bood8P'

     Orbit Database Command Line Tool (c) Mad Penguin Consulting Ltd 2023
     To get started try help register or help for all available commands
none> 
```
So now using the shell on our previous examples, we can easily see what it is we did.
```bash
none> register demo2 /home/gareth/dbdemo/demo
Registered demo2 => /home/gareth/dbdemo/demo
none> use demo2
demo2> show tables
┏━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Table name ┃  Recs ┃ Codec ┃ Depth ┃ Oflow% ┃ Index names                  ┃
┡━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ messages   │ 7     │ ujson │ 1     │ 0.0    │ by_tags                      │
│ people     │ 7     │ ujson │ 1     │ 0.0    │ by_age, by_name, by_name_age │
└────────────┴───────┴───────┴───────┴────────┴──────────────────────────────┘
```
If you read through all of the previous sections you will recall we created two tables (as listed) then went back and seed some indecies. We can now look at that data in the shell with;
```bash
demo2> select messages *
┏━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ text               ┃ tags                      ┃
┡━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ Message - 1        │ ['RED']                   │
│ Message - 2        │ ['RED', 'BLUE']           │
│ Message - 3        │ ['GREEN']                 │
│ Message - 4        │ ['RED', 'GREEN']          │
│ Message - 5        │ ['BLUE', 'GREEN']         │
│ Message - 6        │ ['GREEN']                 │
│ Message - 6        │ ['YELLOW']                │
└────────────────────┴───────────────────────────┘
           Time: 0.0000s => 159089/sec   
```
This is listing our records in natual order. To use the index we can do the following, note that the order is denoted by the value of the lowest value in <b>tags</b>;

```bash
demo2> select messages * index=by_tags
┏━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ text               ┃ tags                      ┃
┡━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ Message - 2        │ ['RED', 'BLUE']           │
│ Message - 5        │ ['BLUE', 'GREEN']         │
│ Message - 3        │ ['GREEN']                 │
│ Message - 4        │ ['RED', 'GREEN']          │
│ Message - 5        │ ['BLUE', 'GREEN']         │
│ Message - 6        │ ['GREEN']                 │
│ Message - 1        │ ['RED']                   │
│ Message - 2        │ ['RED', 'BLUE']           │
│ Message - 4        │ ['RED', 'GREEN']          │
│ Message - 6        │ ['YELLOW']                │
└────────────────────┴───────────────────────────┘
  Time: 0.0001s => 129870/sec (Limited view[10])  
```
And we can utilise the <b>unique</b> command to pull out all the unique values for any given field, so as per the previous discussion a list of unique tags with a count of the number of items tagged with each tag can be generated with;

```bash
demo2> unique messages tags
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━┓
┃ Field name                   ┃ Count           ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━┩
│ BLUE                         │ 2               │
│ GREEN                        │ 4               │
│ RED                          │ 3               │
│ YELLOW                       │ 1               │
└──────────────────────────────┴─────────────────┘
            Time: 0.0001s => 55118/sec    
```
We can look at the structure of a table with <b>example</b>, just bear in mind that because there is no schema then this is based on sampling hence is clearly not an authoratative definition of the fields available, just indicative assuming object types are similar;

```bash
demo2> explain messages
┏━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━┓
┃ Field names    ┃ Field types   ┃ Sample        ┃
┡━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━┩
│ text           │ ['str']       │ Message - 6   │
│ tags           │ ['list']      │ ['YELLOW']    │
└────────────────┴───────────────┴───────────────┘
```
This isn't the full extent of the tool, however it gives a flavour of what it can do. There is also a very extensive help system for each command with examples of input and output. When coupled with autocomplete this makes for a relatively fluid and easy to use console UI.

```python
demo2> help

        help - provide some insight into the available commands and what they do

        help           - this topic
        help [command] - provide help on the selected command

        commands - show select use register explain clear unique analyse dump
                   lexicon match delete import drop fix mode create

        Examples:
        "he" followed by tab will complete to "help", hit space, then tab, and all available
        topics will be displayed on the next line down. Type "se" then tab, then return, and you
        will see the help screen for "select".

        Notes:
        - most components provide 'readline' style tab completion, so when you are faced
          with an explicit choice, hitting tab should list the available choices on the next
          line down. Once you have typed the unique portion of one of those choices, hitting tab
          will auto complete the word.
        - historic auto-complete is also provided, previous commands will pop-up in gray, use the right arrow
          key to select a previous command in it's entirety.
        - all commands should have some help information, more complex commands should include examples
```