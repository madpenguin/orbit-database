#!/usr/bin/env python3
#
#   Script to create a v2 index database from a v1
#
from sys import argv
from pathlib import Path
from orbit_database import Manager, ReadTransaction
from tqdm import tqdm

def error (text):
    print(f'Error: {text}')
    print(f'{argv[0]} <input database path> <output database path>')
    exit()

if __name__ == '__main__':

    if len(argv) < 3:
        error('no databases specified')
    if not Path(argv[1]).exists():
        error(f'unable to locate source: {argv[1]}')
    if Path(argv[2]).exists():
        error(f'target already exists: {argv[2]}')

    manager = Manager()
    inp = manager.database(argv[1])
    print(f'Input version: {inp.index_version}')

    map_size = int(inp.env.info()['map_size'] * 1.2)
    src_path = Path(inp._path) / 'data.mdb'
    stat = src_path.stat()
    print("Map Size: ", map_size)
    # map_size = 1024 * 1024 * 1024 * 64

    out = manager.database(argv[2], config={'map_size': map_size})
    print(f'Output version: {out.index_version}')

    for table_name in inp.tables():
        if table_name[0] in ['-', '_', '@']:
            continue
        source = inp.table(table_name)
        conf = {
            'compression_type': source._compression_type,
            'compression_level': source._compression_level,
            'codec': source._codec,
            'integerkey': source.integerkey
        }
        target = out.table(table_name, **conf)
        iwx = None
        for index_name in source.indexes():
            index = source[index_name]
            conf = {
                'index_name': index_name,
                'func': index.func,
                'duplicates': index.duplicates,
                'iwx': index._iwx,
                'lower': index._lower
                }
            target.ensure(**conf)
            if index._iwx:
                iwx = index
                target_index = target[index_name]

        with tqdm(total=source.records(), colour='green', desc=f'{table_name:25}') as progress:
            for result in source.filter():
                doc = result.doc
                progress.update(1)
                if doc.doc:
                    if iwx:
                        doc.words = iwx.iwx_get_words(result._oid)
                    target.append(doc)

        # with ReadTransaction(out) as txn:
        #     counts = target_index.iwx_counts(txn)
        #     analysis = target_index.iwx_analysis(txn)
        #     print("1>", counts)
        #     print("2>", analysis)


