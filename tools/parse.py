#!/usr/bin/env python3

import ast

with open('../orbit_database/decorators.py', 'r') as io:
    buffer = io.read()
    
tree = ast.parse(buffer)
for item in tree.body:
    if isinstance(item, ast.FunctionDef) or isinstance(item, ast.AsyncFunctionDef):
        print(item)
        