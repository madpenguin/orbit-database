# Using Indexes

#### Orbit Databases are made up of a collection of tables and for each table you can have one or more indexes. By default every table has a unique primary index with an auto-generated key, in addition to this we can create unique and duplicate key secondary indecies. Worth being aware that it is possible to override the default primary key in order to provide your own key, but this is not recommended.

Primary keys are just "there" and can be used to uniquely reference any record in the database. The default <b>ObjectId()</b> object is 14 bytes made up from the record creation date/time, a machine identifier and a process id. For most use-cases this is relatively unimportant however it's worth appreciating that it's composition means that it will be unique across multiple machines when working as a part of a cluster or replicated dataset.

Secondary indecies are a rather more complex proposition allowing you to index content in a number of different ways. Records can be indexed either using a python f-string, or for more granularity using a python function. In both instances the functions are pickled and stored in the index meta-data for persitence. Multi-value indecies are also supported where each record can generate a number of index keys, which is useful in instances such as adding multiple tags to a message and then indexing by tag.

### Getting a record by it's primary key

Probably the easiest operation we have;
```python
>>> from orbit_database import Manager, Doc
>>> database = Manager().database('demo')
>>> people_table = database.table('people')
>>> people = [
    {'name': 'Tom', 'age': 20},
    {'name': 'Dick', 'age': 30},
    {'name': 'Harry', 'age': 25},
    {'name': 'Sam', 'age': 35},
    {'name': 'Sally', 'age': 21},
]
>>> for person in people:
>>>     people_table.append(Doc(person))

>>> first = people_table.first()
>>> print(first.key, first._name)
41d916ac22827be7005732a74ea4 Tom

>>> doc = people_table.get(first.key)
>>> print(doc.key, doc._name)
41d916ac22827be7005732a74ea4 Tom
```

So the <b>first()</b> method can be used to get the first record from the database, and <b>get()</b> can be used to get a specific key, once you know what key you want.

### Creating a secondary index

To start off we're going to create three indecies, one on name, one on age, and one on name + age. The latter has little relevance on the current data set, but we'll add a few extra records just to show it works. All indexes are to allow duplicates, and we're going to make the indecies containing text lower-case, just to avoid and confusion when it comes to searches.

```python
>>> people_table.ensure('by_name', '{name}', duplicates=True, lower=True)
>>> people_table.ensure('by_age', '{age}', duplicates=True)
>>> people_table.ensure('by_name_age', '{name}_{age}', duplicates=True, lower=True)
>>> list(people_table.indexes())
['by_age', 'by_name', 'by_name_age']
```

So now if we try a few indexed queries using our initial dataset, we should get the data in index order, rather than natural or primary key order;

```python
>>> for result in people_table.filter('by_name'):
...     print(f'{result.doc._name:15} => {result.doc._age}')
... 
Dick            => 30
Harry           => 25
Sally           => 21
Sam             => 35
Tom             => 20

>>> for result in people_table.filter('by_age'):
...     print(f'{result.doc._name:15} => {result.doc._age}')
... 
Tom             => 20
Sally           => 21
Harry           => 25
Dick            => 30
Sam             => 35

>>> people_table.append(Doc({'name': 'Harry', 'age': 21}))
>>> people_table.append(Doc({'name': 'Harry', 'age': 18}))

>>> for result in people_table.filter('by_name_age'):
...     print(f'{result.doc._name:15} => {result.doc._age}')
... 
Dick            => 30
Harry           => 18
Harry           => 21
Harry           => 25
Sally           => 21
Sam             => 35
Tom             => 20
```

### Searching an index

So, now we have indexed tables and we can list the data in the order of a specified index, the next thing we need is the ability to select one or more records based on the index and a search criteria. The easiest and most flexible option for this is <b>filter</b>. Filter can accept an upper and lower bound and return all records within those bounds (which are specified as <b>Doc</b> objects) and by default it will return all records >= the lower bound and =< the upper bound. If on the other hand you add "inclusive=False", it will only return matches "within" the bounds. So for example;

```python
>>> limit = Doc({'name': 'Harry'})
>>> for result in people_table.filter('by_name', lower=limit, upper=limit):
...     print(result.doc._name, result.doc._age)
... 
Harry 25
Harry 21
Harry 18
```
But;
```python
>>> for result in people_table.filter('by_name', lower=limit, upper=limit, inclusive=False):
...     print(result.doc._name, result.doc._age)
... 
```
So there is nothing "between" "Harry" and "Harry". Moving on one step, this also works for partial key matches, consider if we want all names beginning with D thru H (notice that because we created the indecies initially with "lower=True" they are effectively case insensitive);
```python
>>> lower = Doc({'name': 'd'})
>>> upper = Doc({'name': 'hz'})
>>> for result in people_table.filter('by_name', lower=lower, upper=upper):
...     print(result.doc._name, result.doc._age)
... 
Dick 30
Harry 25
Harry 21
Harry 18
```

### The result Object

So you'll notice that filter always returns a <b>result</b> object rather than a <b>Doc</b> object, hopefully we can now show why. Consider the following;

```python
>>> limit = Doc({'name': 'Harry'})
>>> for result in people_table.filter('by_name', lower=limit, upper=limit):
...     print(result.key, result.count)
... 
b'harry' 3
b'harry' 3
b'harry' 3
```
So, we can get the key and the number of occurrences of that key for very little 'cost', we're only reading the index and because we've not tried to access the 'doc' object, it's not actually accessed the data record for the result. So because keys are typically small and because keys store an actual binary version of they key (rather than a JSON object), they are many (many) times quicker than reading the actual records, which gives scope for many performance optimisations in your code. In particular;

```python
>>> for result in people_table.filter('by_name', lower=limit, upper=limit, suppress_duplicates=True):
...     print(result.key, result.count)
... 
b'harry' 3
```

So, if you were in add a tag to a million records, then add an index based on "tag", you could then read the number of records with "tag" attached with a single index lookup, i.e. without even needing to read a single database record (!) 

### Performance indicators

Just to put this into perspective, in Orbit Communicator, down the left of the screen is a list of folders, where each folder is implemented as a tag. The count next to each tag is the number of records actually tagged with that tag. So there are a quarter of a million records and about 20 tags, and about 450,000 entries in the tags index. To produce this list in SQL would be something like "SELECT COUNT(DISTINCT(tag)) FROM messages", Orbit Communicator times each call and currently it's reporing that generating the folder list takes 1/4500th of a second. My memories of MySQL's SELECT DISTINCT performance make me think 4500 times a second looks pretty good compared to traditional SQL technology.

### Index rebuilds and progress indicators

If you change the definition of an index, then when it commits the new index definition it should try to regenerate the index at the same time. If there is a chance another process might be trying to use a given table when you change the index definition, it is advisable to wrap the "ensure" in with a write transaction such that if the reindex fails, it will abort the transaction and revert to the old index and index definition.

If your index definition has not changed but you want to force it to regenerate the index anyway, simply add "force=True" to your "ensure" statement.

If you want to "watch" the reindex happen, ensure had a "progress" parameter which if supplied as a function definition, will be called as the reindexer moves through the table. The typical use for this is with TQDM although it could just as easily drive a GUI or web based progress bar. So for example;

```bash
$ pip install tqdm
Collecting tqdm
  Downloading tqdm-4.65.0-py3-none-any.whl (77 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 77.1/77.1 KB 3.0 MB/s eta 0:00:00
Installing collected packages: tqdm
Successfully installed tqdm-4.65.0
```
And using our initial dataset of 5 people records;

```python
>>> with tqdm(total=people_table.records(), unit='') as progress:
...     people_table.ensure('by_name', '{name}', duplicates=True, lower=True, progress=progress, force=True)
... 
100%|█████████████████████████████████████████████████████████████████████████████| 5/5 [00:00<00:00, 8901.32/s]
```

### Advanced Indexing

Now we're getting to the really fun stuff. In addition to python f-strings, we can also specify python functions in order to generate index keys. Whereas you don't really want to be importing modules or using external data in these functions (because it kills their portability a little), you can pretty much do 
whatever else you need to do.

A nice example as mentioned earlier is multi-valued indecies and tags. In this instance we need the index to be a function that will take the record and return a list of index entries, so one record == multiple index entries. In the context of tags, this means you can add multiple (different) tags to a message, then count the number of tags associated with each message, or lookup all the messages associated with a tag. So let's see what that looks like;

```python
>>> from orbit_database import Manager, Doc
>>> database = Manager().database('demo')
>>> messages = database.table('messages')
>>> src = [
    {'text': 'Message # 1', 'tags': ['RED']},
    {'text': 'Message # 2', 'tags': ['RED', 'BLUE']},
    {'text': 'Message # 3', 'tags': ['GREEN']},
    {'text': 'Message # 4', 'tags': ['RED', 'GREEN']},
    {'text': 'Message # 5', 'tags': ['BLUE', 'GREEN']},
    {'text': 'Message # 6', 'tags': ['GREEN']},
    {'text': 'Message # 6', 'tags': ['YELLOW']},
]
>>> for message in src:
...     messages.append(Doc(message))
```
So now we have our data, we need to create in index which is going to return multiple values based on 'tags';

```python
>>> messages.ensure(
...     'by_tags',
...     duplicates=True,
...     func="""def func(doc): return [t.encode() for t in doc.get("tags",[]) if t]""")
```

Now if you're not comfortable with list comprehensions, you might need to do a little reading at this point, otherwise you will see that the function specified will return a list of binary keys, one for each entry in 'tags'. So we can now do;

```python
>>> for result in messages.filter('by_tags'):
...     print(result.key, result.doc.doc)
... 
b'BLUE' {'text': 'Message # 2', 'tags': ['RED', 'BLUE']}
b'BLUE' {'text': 'Message # 5', 'tags': ['BLUE', 'GREEN']}
b'GREEN' {'text': 'Message # 3', 'tags': ['GREEN']}
b'GREEN' {'text': 'Message # 4', 'tags': ['RED', 'GREEN']}
b'GREEN' {'text': 'Message # 5', 'tags': ['BLUE', 'GREEN']}
b'GREEN' {'text': 'Message # 6', 'tags': ['GREEN']}
b'RED' {'text': 'Message # 1', 'tags': ['RED']}
b'RED' {'text': 'Message # 2', 'tags': ['RED', 'BLUE']}
b'RED' {'text': 'Message # 4', 'tags': ['RED', 'GREEN']}
b'YELLOW' {'text': 'Message # 6', 'tags': ['YELLOW']}
```

Or to get our count of the number of messages tagged by each tag (without actually reading any records) we can do;

```python
>>> for result in messages.filter('by_tags', suppress_duplicates=True):
...     print(result.key, result.count)
... 
b'BLUE' 2
b'GREEN' 4
b'RED' 3
b'YELLOW' 1
```

Then to get all the messages tagged with say "BLUE" we can do;

```python
>>> limit = Doc({'tags': ['BLUE']})
>>> for result in messages.filter('by_tags', lower=limit, upper=limit):
...     print(result.doc.doc)
... 
{'text': 'Message # 2', 'tags': ['RED', 'BLUE']}
{'text': 'Message # 5', 'tags': ['BLUE', 'GREEN']}
```

Or to get all the records tagged with either YELLOW or RED we could do;

```python
>>> limit = Doc({'tags': ['YELLOW', 'RED']})
>>> for result in messages.filter('by_tags', lower=limit, upper=limit):
...     print(result.doc.doc)
... 
{'text': 'Message # 6', 'tags': ['YELLOW']}
{'text': 'Message # 1', 'tags': ['RED']}
{'text': 'Message # 2', 'tags': ['RED', 'BLUE']}
{'text': 'Message # 4', 'tags': ['RED', 'GREEN']}
```

And the real power here is the ability to run many thousands of such queries per second on datasets containing millions of records. But going back to the "func=" in "ensure", you can easily contruct a key from any of the mutated components of the record, whether single or multi-value. (All you need to do for multi-value is return a list rather than a single item)

Indexes are the real power behind the database and are surprisingly quick, at the end of day the amount of Python involved in reading (and writing) index entries is relatively small so the speedups available due to not being tied to SQL quickly counter-balance not being entirely written in "C". Bear in mind that LMDB is doing the vast majority of the work and LMDB IS written in "C".