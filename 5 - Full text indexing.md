# Full Text Indexing in Orbit

#### In reality this is a very specific type of full-text indexing which was initially designed to index email messages, although it should work well for other use-cases. It is not a blanket or automatic 'index everything' solution as these are rarely efficient or useful in real-world environments.

### What sort of index does it use?

This could be considered an unusual solution as it uses something called an <b>inverted word index</b>. This isn't a new concept as such, but a the other hand a performant implementation in Python to drive a database search, just might be. Things to be aware of;

* We make a concious choice about which words we index
* We preclude words with no meaning or an insignificant length
* We write the indexed words as separate metadata 
* The index contains a lexicon of all indexed words
* The index itself is as list, for each word, of which documents it appears in

##### <b>Note</b>; Generating the valid word list 'can' take a significant amount of time and cause re-indexing issues if done on-the-fly, hence the words are stored in metadata. Given storage is cheap and time is expensive, many old paradigms revolving around the minimisation of data storage are, these days, often very outdated.

Now at first sight, this might sound horrendous. A list of documents for each word is going to consume an impossinle amount of storage. However. This list is stores as a 'sparse bitmap', so the amount of 
storage requres is far less than one might imagine and the storage usage is determined more by the number of words than by the number of document stored. In terms of searching for a combination of words, it's a simple case of;

* Look up the words in the lexicon
* Look up the bitmap associated with each word
* Logical and / or the bitmaps to get the required match combination
* The look up the bitmap offsets in the bitmap index to get the primary keys of matching records

So, this is 100% driven by direct hashed lookups and logical bitmap operations .. so in theory it's pretty quick. In practice, performance depends on the implementation of the sparse bitmap lookup and the number of results it has to compile. The current implementation is pretty good and works well for result sets in the hundreds of thousands. If you only ask it for "the first 10,000" results from any search, then millions of records in the search index is not a problem, nor is document size as searching happens on the lexicon. 

<img style="background-color: white; padding: 2em;border-radius:6px" src="images/ftx.svg" />

### A practical example

So what does this actually look like in practice? Well, if we try to do something really simple to start off with. If we use a very simple database with one table as follows;

```bash
none> use starwars
starwars> show tables
┏━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━━┳━━━━━━━━━━━━━┓
┃ Table name ┃  Recs ┃ Codec ┃ Depth ┃ Oflow% ┃ Index names ┃
┡━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━━╇━━━━━━━━━━━━━┩
│ actors     │ 8     │ ujson │ 1     │ 0.0    │ by_name     │
└────────────┴───────┴───────┴───────┴────────┴─────────────┘
starwars> explain actors
┏━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Field names      ┃ Field types         ┃ Sample                           ┃
┡━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ name             │ ['str']             │ Baby Yoda                        │
│ home_planet      │ ['str', 'NoneType'] │ Dagobah                          │
│ type             │ ['int']             │ 2                                │
│ friends          │ ['list']            │ ['41d812f1768fe6af005732a799a6'] │
│ appears_in       │ ['list']            │ [5, 6, 4]                        │
│ primary_function │ ['str']             │ Astromech                        │
└──────────────────┴─────────────────────┴──────────────────────────────────┘

```
So we have three fields which contain textual information, let's try to generate a full-text index based on these three fields. To start off with, let's create the index;

```python
#!/usr/bin/env python
from orbit_database import Manager, Doc
db = Manager().database('starwars')
actors = db.table('actors')
actors.ensure('by_word', iwx=True)
```

If we run this then take another look at the table we see;

```bash
none> use starwars
starwars> show tables
┏━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━━┳━━━━━━━━━━━━━━━━━━┓
┃ Table name ┃  Recs ┃ Codec ┃ Depth ┃ Oflow% ┃ Index names      ┃
┡━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━━╇━━━━━━━━━━━━━━━━━━┩
│ actors     │ 8     │ ujson │ 1     │ 0.0    │ by_name, by_word │
└────────────┴───────┴───────┴───────┴────────┴──────────────────┘
```

Next we need to populate the 'words' metadata, although there is now an index, when the records were initially written, the words attribite wasn't populated so nothing will currently be in the index. So;

```python
from collections import Counter
for result in actors.filter():
    actor = result.doc
    words = []
    for field in ('name', 'home_planet', 'primary_function'):
        if field in actor and actor[field]:
            words += actor[field].split(' ')

    actor.words = Counter(words)
    actors.save(actor)
```

So the specification of what needs to be written in the words field is a list of words wrapped in a Counter object. (where Counter is a class from the python standard library) This reduces the list to a unique set of words (removed duplicates) and changes each word into a tuple of (words, frequency). This has a dual function, it removes duplicate words and help us track the number of occurrences of each word.

Typically you will need to wrap whatever it is in your application that updates a table with a full-text-index, with a function that extracts the words you want and stores then in the 'words' attribute of the record, before you write the record. We can now query the lexicon in the shell, and use the match command to reference the actual matching document, then dump to see the match;

```bash
starwars> show indexes actors
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━┳━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━━━┓
┃ Table name                   ┃ Index name ┃ Entries ┃ Dups  ┃ Key             ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━╇━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━━━┩
│ actors                       │ by_name    │ 8       │ False │ {name}          │
│ actors                       │ by_word    │         │       │ Full Text Index │
│ 41d919e3186caffa005732a7f818 │ lexicon    │ 21      │       │ 12.0KiB         │
│ 41d919e3186cb008005732a7f818 │ docindx    │ 8       │       │ 12.0KiB         │
│ 41d919e3186cb00e005732a7f818 │ docrindx   │ 8       │       │ 12.0KiB         │
│ 41d919e3186cb013005732a7f818 │ bitmap     │ 21      │       │ 40.0KiB         │
│ 41d919e3186cb018005732a7f818 │ words      │ 8       │       │ 12.0KiB         │
└──────────────────────────────┴────────────┴─────────┴───────┴─────────────────┘

starwars> lexicon actors by_word A
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━┓
┃ Term                        ┃ Count            ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━┩
│ Alderaan                    │ 1                │
│ Astromech                   │ 1                │
└─────────────────────────────┴──────────────────┘
            Time: 0.0003s => 7632/sec

starwars> match actors by_word Alderaan
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ _id                                            ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ b'41d812f1768fe804005732a799a6'                │
└────────────────────────────────────────────────┘
            Time: 0.0002s => 4504/sec             

starwars> dump actors 41d812f1768fe804005732a799a6
{
    "name": "Leia Organa",
    "home_planet": "Alderaan",
    "type": 1,
    "friends": [
        "41d812f1768fe6af005732a799a6",
        "41d812f1768fe804005732a799a6",
        "41d812f1768fe88e005732a799a6",
        "41d812f1768fe8d1005732a799a6"
    ],
    "appears_in": [
        4,
        5,
        6
    ],
    "primary_function": "Human"
}
```

Note that in this instance we have not excplicitly lowercased things, so currently the index and searching are case sensitive. Just to explain a litte of the detail in "show indexes", the full-text index comprises of a number of sub-tables, notably the lexicon has one entry per unique word stores, and words should be the number of documents indexed, i.e. the number of records in the table.