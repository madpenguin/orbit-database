# Getting Started with Orbit Database

#### The Database is effectively a Python library which can either be used with the Orbit Framework, or in it's own right as a stand-alone database. If you are working with an Orbit Framework application or indeed just need a database library, this documentation is for you. If on the other hand you are writing code that forms an Orbit Component or runs directly within Orbit, take a look at the Orbit Framework ORM documentation.

The database acquires it's base features and characteristics from the LMDB library which is used to hold the data stored in the database. You may find the [LMDB documentation](https://lmdb.readthedocs.io/en/release/) useful background information, but to get going it's probably more than you need to worry about initially.

To get started, you'll first need to create a virtual Python environment with the tool of your choosing, our current preference is <b>pyenv</b>, then install orbit database, so;
```bash
$ pyenv virtualenv dbdemo
$ pyenv activate dbdemo
(dbdemo):~$ pip install orbit-database
$ pip install orbit-database
Collecting orbit-database
  Downloading orbit_database-0.99.91-py3-none-any.whl (45 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 45.8/45.8 KB 1.7 MB/s eta 0:00:00
Collecting posix-ipc<2.0.0,>=1.1.1
  Using cached posix_ipc-1.1.1.tar.gz (94 kB)
  Preparing metadata (setup.py) ... done
Collecting lmdb<2.0.0,>=1.4.1
  Downloading lmdb-1.4.1-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (299 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 299.2/299.2 KB 6.0 MB/s eta 0:00:00
Using legacy 'setup.py install' for posix-ipc, since package 'wheel' is not installed.
Installing collected packages: posix-ipc, lmdb, orbit-database
  Running setup.py install for posix-ipc ... done
Successfully installed lmdb-1.4.1 orbit-database-0.99.91 posix-ipc-1.1.1
```
We also need to choose a serialiser and we recommend a smart logging library;
```bash
$ pip install ujson
Collecting ujson
  Downloading ujson-5.7.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (52 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 52.8/52.8 KB 2.1 MB/s eta 0:00:00
Installing collected packages: ujson
Successfully installed ujson-5.7.0
$ pip install loguru
Collecting loguru
  Downloading loguru-0.7.0-py3-none-any.whl (59 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 60.0/60.0 KB 2.4 MB/s eta 0:00:00
Installing collected packages: loguru
Successfully installed loguru-0.7.0
```
A number of serialised are generally supported but currently you will need <b>ujson</b> for full-text indexing support.

The next thing to do would be to create a database. LMDB in our chosen configuration stores a database in a dedicated folder as two <b>.mdb</b> files. By default, all these files are created automatically if the database does not exist, so be careful with your spelling. As with most options in Orbit Database, the defaults will be the easiest to use, but you will be able to fine-tune for production.

So, if we go ahead and create our first database;
```python
$ python
Python 3.10.6 (main, Mar 10 2023, 10:55:28) [GCC 11.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from orbit_database import Manager
>>> database = Manager().database('demo')
```
We're now running, in the background you will find it's created a folder called "demo" and within that folder there will be two files called <b>data.mdb</b> and <b>lock.mdb</b>. This is where all your valuable data is going and this is what you need to keep safe.

##### <b>Note</b>: databases are user-specific, whereas they can be shared between multiple processes very efficiently, they are designed to be referenced by a single user. If you need different users to access the contends of they database, this should be done via your application, rather than letting them access the database file directly.

We can now go ahead and start adding data to the database, for example;
```python
>>> from orbit_database import Doc
>>> people_table = database.table('people')
>>> people = [
    {'name': 'Tom', 'age': 20},
    {'name': 'Dick', 'age': 30},
    {'name': 'Harry', 'age': 25},
    {'name': 'Sam', 'age': 35},
    {'name': 'Sally', 'age': 21},
]
>>> for person in people:
>>>     people_table.append(Doc(person))
>>> people_table.records()
5
```
##### <b>Note</b>: The <b>Doc</b> object is used to encapsulate data that we write to the database, and when we read from the database, ultimately it always returns a <b>Doc</b> object. (although <b>filter</b> returns a <b>filteresult</b> object, which then lazy-loads a Doc object)

### The 'Doc' object

Probably worth explaining the doc object at this point. At a very low level the database reads and writes keys, and for each key, an associated value which is essentially a binary string. To make life a little easier, when you write a 'doc' object it automatically converts the object to a binary string for you, and when you read the database, it converts the binary string back into a 'doc' object. The Doc object itself then has a number of useful attribites;

* key - which is the key associated with the record
* doc - which is the data in the form of a python dictionary
* _   - which can be used to access individual fields within the record, so in the exampel above, doc._name would reference the name field, and doc._age would reference the age field.These fields are read and write and can be used interchagably with dictionary notation, so doc['name'] and doc['age'] also work.

So far so good, but what about getting the data out? There are a number of mechanisms however the Swiss army knife of data recovery is the <b>filter</b> method which can do most of the things you will want. So in this case;
```python
>>> for result in people_table.filter():
...     print(result.doc.doc)
... 
{'name': 'Tom', 'age': 20}
{'name': 'Dick', 'age': 30}
{'name': 'Harry', 'age': 25}
{'name': 'Sam', 'age': 35}
{'name': 'Sally', 'age': 21}
```
The filter method returns a <b>result</b> object which is a <b>lazy load</b> object. (which becomes very userful and efficient when using indexes as it recovers the index entry and not the actual data). Once you have this, accessing the <b>doc</b> method will recover the actual data, and from there the <b>doc</b> method (again) will format the entire method as a json string. Each field within the doc object can also be recovered via the underscore operator, for example;
```python
>>> for result in people_table.filter():
>>>     person = result.doc
>>>>    print(f"{person._name:12} {person._age:2}")
...
Tom          20
Dick         30
Harry        25
Sam          35
Sally        21
```
##### <b>Note</b>: it is actualy 'good form' to assign result.doc within the loop. If you are only using <b>result</b> and it's derivatives within the for loop (as in this case) it's not needed, if however you are going to store result for later use, once outside of the loop <b>result</b> will lose the ability to resolve <b>doc</b>.

### Where are all the Transactions?

As we said, it's an ACID compliant database, and as we said that by default we're going to present the easy option as the default. So in the cases above, as we've not explicitly set up transactions, the code is automatically creating a read-only transaction for each database operation. So in the case of filter, the filter method will be running within an isolated read-only transaction, it will just be an auto-genrated transaction. The same goes for <b>append</b>, a one-off write transaction is generated on the fly if we don't explicitly create and specify one.

So, what if I want to add all my records at once, within a single write transaction?
```python
>>> people_table.empty()
>>> people_table.records()
0
>>> from orbit_database import WriteTransaction
>>> with WriteTransaction(database) as txn:
...     for person in people:
...             people_table.append(Doc(person), txn=txn)
... 
>>> people_table.records()
5
```

### Updating records

So when it comes to updating records, again it's very straightforward and for most cases the defaults will suffice. Pretty much all methods accept a "txn" parameter, so if you want to control the transaction yourself, all you need to do is wrap your updates in a "with WriteTransaction as txn" and add the txn parameter to any updates (line append) that you make.

Let say in this instance we want to make everyone a year older, we would do;
```python
for result in people_table.filter():
    person = result.doc
    person._age += 1
    people_table.save(person)

>>> for result in people_table.filter():
...     person = result.doc
...     print(f"{person._name:12} {person._age:2}")
... 
Tom          21
Dick         31
Harry        26
Sam          36
Sally        22
```

So there's the introduction, how to create a database, how to insert some data, how to get the data back and how to update it. (and how to use transactions) This all operates at a basic / sequential table level, take a look at the 'Using Indexes' section for more advanced access.