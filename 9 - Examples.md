# Example code

There will be a compilation of examples curated here, although for now the best examples are the orbit components which can be found in the 'madpenguin' repository. (and all the other snippets found in this directory) There are a couple of examples that have been carried through from previous versions of the database, interestingly after just trying the "benchmark" code, while not particularly elegant it ran first time after changing only the import statement. (after being authored 4 years ago for a much older version of the database)

See 'benchmark.py' in the *examples* folder (these results running within development VM);
```python
$ python3 examples/benchmark.py 
2023-07-20 11:16:44.800 | INFO     | __main__:main:82 - ** SINGLE Threaded benchmark **
2023-07-20 11:16:44.801 | INFO     | __main__:main:88 - No indexing
2023-07-20 11:16:44.905 | INFO     | __main__:persec:35 - Append: speed=48005/sec | 0:5000
2023-07-20 11:16:44.984 | INFO     | __main__:persec:35 - Append: speed=64031/sec | 5000:5000
2023-07-20 11:16:45.066 | INFO     | __main__:persec:35 - Append: speed=61105/sec | 10000:5000
2023-07-20 11:16:45.067 | INFO     | __main__:main:98 - Indexed by sid, day, hour
2023-07-20 11:16:45.215 | INFO     | __main__:persec:35 - Append: speed=34062/sec | 0:5000
2023-07-20 11:16:45.371 | INFO     | __main__:persec:35 - Append: speed=32030/sec | 5000:5000
2023-07-20 11:16:45.534 | INFO     | __main__:persec:35 - Append: speed=30788/sec | 10000:5000
2023-07-20 11:16:45.537 | INFO     | __main__:main:111 - Compound index
2023-07-20 11:16:45.661 | INFO     | __main__:persec:35 - Append: speed=40708/sec | 0:5000
2023-07-20 11:16:45.785 | INFO     | __main__:persec:35 - Append: speed=40170/sec | 5000:5000
2023-07-20 11:16:45.906 | INFO     | __main__:persec:35 - Append: speed=41400/sec | 10000:5000
2023-07-20 11:16:45.908 | INFO     | __main__:main:121 - Linear scan through most recent index
2023-07-20 11:16:45.985 | INFO     | __main__:persec:35 - Read..: speed=194693/sec | 0:15000
```
Anothe old example from the *examples* folder, again ran first time (I mention this simply to show the stability of the functionality across many years of development);
```python
$ python3 examples/people.py 
2023-07-20 11:24:01.193 | INFO     | __main__:open:65 - ** SETUP **
2023-07-20 11:24:01.195 | INFO     | __main__:simple_scan:82 - ** SIMPLE SCAN **
2023-07-20 11:24:01.195 | INFO     | __main__:simple_scan:84 - Tom Jones is 21 years old
2023-07-20 11:24:01.195 | INFO     | __main__:simple_scan:84 - Squizzey is 3000 years old
2023-07-20 11:24:01.195 | INFO     | __main__:simple_scan:84 - Fred Bloggs is 45 years old
2023-07-20 11:24:01.195 | INFO     | __main__:simple_scan:84 - John Doe is 0 years old
2023-07-20 11:24:01.196 | INFO     | __main__:simple_scan:84 - John Smith is 40 years old
2023-07-20 11:24:01.196 | INFO     | __main__:scan_by_name:90 - ** SIMPLE SCAN BY NAME **
2023-07-20 11:24:01.196 | INFO     | __main__:scan_by_name:92 - Fred Bloggs sorted alphabetically
2023-07-20 11:24:01.196 | INFO     | __main__:scan_by_name:92 - John Doe sorted alphabetically
2023-07-20 11:24:01.196 | INFO     | __main__:scan_by_name:92 - John Smith sorted alphabetically
2023-07-20 11:24:01.196 | INFO     | __main__:scan_by_name:92 - Squizzey sorted alphabetically
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_name:92 - Tom Jones sorted alphabetically
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_age:98 - ** SIMPLE SCAN BY AGE**
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_age:100 - 0 - John Doe in ascending order of age
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_age:100 - 21 - Tom Jones in ascending order of age
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_age:100 - 40 - John Smith in ascending order of age
2023-07-20 11:24:01.197 | INFO     | __main__:scan_by_age:100 - 45 - Fred Bloggs in ascending order of age
2023-07-20 11:24:01.198 | INFO     | __main__:scan_by_age:100 - 3000 - Squizzey in ascending order of age
2023-07-20 11:24:01.198 | INFO     | __main__:scan_with_filter:104 - ** SIMPLE SCAN WITH A FILTER**
2023-07-20 11:24:01.198 | INFO     | __main__:scan_with_filter:106 - Fred Bloggs is 45 years old (filtered age>40)
2023-07-20 11:24:01.198 | INFO     | __main__:scan_with_filter:106 - Squizzey is 3000 years old (filtered age>40)
2023-07-20 11:24:01.198 | INFO     | __main__:close:112 - ** TEARDOWN **
```
