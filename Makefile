VERSION=$(shell grep __version__ orbit_database/__init__.py |cut -d'"' -f2)
URL=https://img.shields.io/badge/Version-${VERSION}-teal?style=flat -o images/version.svg

version:
	@echo ${URL}
	@curl ${URL}

publish_local:
	@echo "publishing ..."
	@./scripts/roll_version.py
	@poetry publish --build --repository borg

publish:
	@echo "publishing ..."
	@./scripts/roll_version.py
	@poetry build && poetry publish
	@curl ${URL}

testall:
	PYTHONPATH=. pytest -s

test_doc:
	PYTHONPATH=. pytest -s tests/test_doc.py

test_filter:
	PYTHONPATH=. pytest -s tests/test_filter.py

backup:
	tar cvfz /tmp/orbit-database.tgz \
                --exclude=node_modules \
                --exclude=node_modules.old \
                --exclude="*.mdb" \
                --exclude="dist" \
                --exclude="build" \
                --exclude=".cache" \
                --exclude="*.log" \
                --exclude="*.zip" \
                --exclude=".random_errors" \
                --exclude=".pytest*" \
                --exclude="*__pycache__*" \
                --exclude=".attic" \
                --exclude=".tox" \
                --exclude=".venv"  \
                --exclude=".crap"  \
                --exclude="htmlcov"  \
                --exclude=".git" \
                --exclude=".thumbnail_cache" \
                --exclude=".parts_cache" \
                .
