"""

    Copyright (c) 2021 - Gareth Bult

"""
import pytest
import sys
from collections import Counter
from tempfile import TemporaryDirectory
from pathlib import Path
from orbit_database import WriteTransaction, Manager, Doc, InvertedWordIndex, SerialiserType, Bitmap, Lexicon, ReadTransaction
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import message_from_string
from email.policy import default
from nltk import word_tokenize
from bs4 import BeautifulSoup
from re import sub
from struct import pack, unpack
import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
STOP = set(stopwords.words('english'))
STOP.add('none')
DATABASE = '.test_database'


@pytest.fixture(params=[1,2])
def version (request):
    return request.param


@pytest.fixture
def database(version):
    with TemporaryDirectory() as dir:
        db_file_name = (Path(dir) / DATABASE).as_posix()
        config = {
            'writemap': False,
            'map_size': 1024 * 1024,
            'auto_resize': True,
            'version': version
        }
        db = Manager().database('db', db_file_name, config=config)
        table = db.table('people')
        table.ensure('full_text', iwx=True)
        yield db


@pytest.fixture
def people (database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    words = ['1999', '20internet', 'able', 'agreements', 'another', 'anywhere', 'apply', 'back', 'back', 'beyond', 'binding', 'bug', 'bult',
        'bult', 'bult', 'bult', 'bye', 'case', 'cleared', 'cleared', 'click', 'combination', 'come', 'conditions', 'contained',
        'contractually', 'copy', 'couple', 'dad', 'dad', 'dad', 'default', 'descriptions', 'director', 'door', 'else', 'email', 'eve',
        'eve', 'express', 'express', 'format', 'forward', 'frontier', 'frontier', 'frontier', 'ftech', 'ftech.net', 'gareth', 'gareth',
        'gareth', 'gareth', 'get', 'get', 'get', 'get', 'got', 'gottit', 'hoping', 'imceaex', 'instead', 'interesting', 'internet',
        'january', 'kbult.ftech.co.uk', 'kbult.ftech.co.uk', 'keith', 'keith', 'keith', 'keith', 'like', 'literature', 'look', 'looks',
        'looks', 'ltd', 'made', 'made', 'mail', 'mailbox', 'main', 'means', 'menu', 'message', 'message', 'message', 'new', 'next',
        'office', 'office', 'one', 'option', 'original', 'outlook', 'outlook', 'pick', 'present', 'product', 'recipients', 'relied',
        'representations', 'research', 'round', 'sales', 'sales', 'search', 'see', 'seein', 'sent', 'server', 'service', 'services',
        'set', 'smtp', 'something', 'soon', 'statements', 'stationary', 'stationary', 'stationary', 'subject', 'subject', 'sunday',
        'sunday', 'sunday', 'sure', 'terms', 'therein', 'time', 'times', 'tomorrow', 'tried', 'try', 'upon', 'upstairs', 'use', 'using',
        'wait', 'waiting', 'way', 'weeks', 'well', 'whether', 'within', 'word', 'world', 'would', 'yup']

    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=Counter(words))
    people.append(doc)
    doc = Doc(raw, words=Counter(words))
    people.append(doc)
    return people


def mime_decoder(raw, envelope=Doc()):
    message = message_from_string(raw, policy=default)
    body = message.get_body(preferencelist=('html', 'plain'))
    text = body.get_payload(decode=True)
    if isinstance(text, bytes):
        text = text.decode(errors='ignore')
    assert body.get_content_subtype() == 'html', 'bad data'

    text = BeautifulSoup(text, 'lxml').get_text(separator=" ")
    if not len(text):
        text = BeautifulSoup(text, 'lxml').get_text(separator=" ").strip()
    if envelope:
        text = f'{envelope._subject} {envelope._source} {text}'.strip()
    text = word_tokenize(
        sub(r'&\w+;\s?', '', sub('[^0-9a-zA-Z]+', ' ', text).lower().strip())
    )
    return Counter([word for word in filter(lambda word: len(word) > 2 and word not in STOP, text)])

def test_create_non_full_text_instance():
    index = InvertedWordIndex()
    assert not index._iwx, 'index setup failed'

def test_create_full_text_instance():
    index = InvertedWordIndex(config={'iwx': True})
    assert index._iwx, 'index setup failed'

def test_delete_record(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=mime_decoder(raw))

    people.append(doc)
    dump = people['full_text'].dump(debug=False)

    assert dump['lexicon'] == [
        ['abc', 2],
        ['duplicate', 1],
        ['hello', 3],
        ['test', 1],
        ['world', 2],
        ['xxx', 4],
    ], 'dump mismatch'

    people.delete(doc)
    dump = people['full_text'].dump(debug=False)
    assert dump['lexicon'] == [
        ['abc', 0],
        ['duplicate', 0],
        ['hello', 0],
        ['test', 0],
        ['world', 0],
        ['xxx', 0],
    ], 'dump mismatch'

def test_update_record(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(words=mime_decoder(raw), dat=raw)
    people.append(doc)
    doc.words.update(['extra!'])
    people.save(doc)
    dump2 = people['full_text'].dump(debug=False)
    assert dump2['lexicon'][2][0] == 'extra!'

def test_mime_decoder():
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    text = "Hello World Text"
    html = """<html><body><p>Hello World Html</p></body></html>"""
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    message.attach(part1)
    message.attach(part2)
    words = mime_decoder(message.as_string())
    assert [word for word in words] == ['hello', 'world', 'html'], 'mime decoder failed'

def test_create_index_entry(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=mime_decoder(raw))
    people.append(doc)

    raw = people.get(doc.oid)
    assert raw.dat == message.as_string(), 'input output mismatch'

    dump = people['full_text'].dump(debug=False)

    assert dump['lexicon'] == [
        ['abc', 2],
        ['duplicate', 1],
        ['hello', 3],
        ['test', 1],
        ['world', 2],
        ['xxx', 4]
    ], 'dump mismatch'

def test_delete_in_detail(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    words = ['1999', '20internet', 'able', 'agreements', 'another', 'anywhere', 'apply', 'back', 'back', 'beyond', 'binding', 'bug', 'bult',
        'bult', 'bult', 'bult', 'bye', 'case', 'cleared', 'cleared', 'click', 'combination', 'come', 'conditions', 'contained',
        'contractually', 'copy', 'couple', 'dad', 'dad', 'dad', 'default', 'descriptions', 'director', 'door', 'else', 'email', 'eve',
        'eve', 'express', 'express', 'format', 'forward', 'frontier', 'frontier', 'frontier', 'ftech', 'ftech.net', 'gareth', 'gareth',
        'gareth', 'gareth', 'get', 'get', 'get', 'get', 'got', 'gottit', 'hoping', 'imceaex', 'instead', 'interesting', 'internet',
        'january', 'kbult.ftech.co.uk', 'kbult.ftech.co.uk', 'keith', 'keith', 'keith', 'keith', 'like', 'literature', 'look', 'looks',
        'looks', 'ltd', 'made', 'made', 'mail', 'mailbox', 'main', 'means', 'menu', 'message', 'message', 'message', 'new', 'next',
        'office', 'office', 'one', 'option', 'original', 'outlook', 'outlook', 'pick', 'present', 'product', 'recipients', 'relied',
        'representations', 'research', 'round', 'sales', 'sales', 'search', 'see', 'seein', 'sent', 'server', 'service', 'services',
        'set', 'smtp', 'something', 'soon', 'statements', 'stationary', 'stationary', 'stationary', 'subject', 'subject', 'sunday',
        'sunday', 'sunday', 'sure', 'terms', 'therein', 'time', 'times', 'tomorrow', 'tried', 'try', 'upon', 'upstairs', 'use', 'using',
        'wait', 'waiting', 'way', 'weeks', 'well', 'whether', 'within', 'word', 'world', 'would', 'yup']

    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=Counter(words))
    people.append(doc)
    dump = people['full_text'].dump(debug=False)

    terms = "within world"
    count, results = people.match('full_text', terms.lower().strip().split(" "))
    assert count == 1

    cwords = Counter(words)
    dwords = []
    for word, count in dump['lexicon']:
        assert cwords[word] == count
        dwords.append(word)

    assert set(cwords) == set(dwords)

    people.delete(doc)

    dump = people['full_text'].dump(debug=False)
    dwords = []
    for word, count in dump['lexicon']:
        assert count == 0

    count, results = people.match('full_text', terms.lower().strip().split(" "))
    assert count == 0

def test_lexicon(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    words = ['1999', '20internet', 'able', 'agreements', 'another', 'anywhere', 'apply', 'back', 'back', 'beyond', 'binding', 'bug', 'bult',
        'bult', 'bult', 'bult', 'bye', 'case', 'cleared', 'cleared', 'click', 'combination', 'come', 'conditions', 'contained',
        'contractually', 'copy', 'couple', 'dad', 'dad', 'dad', 'default', 'descriptions', 'director', 'door', 'else', 'email', 'eve',
        'eve', 'express', 'express', 'format', 'forward', 'frontier', 'frontier', 'frontier', 'ftech', 'ftech.net', 'gareth', 'gareth',
        'gareth', 'gareth', 'get', 'get', 'get', 'get', 'got', 'gottit', 'hoping', 'imceaex', 'instead', 'interesting', 'internet',
        'january', 'kbult.ftech.co.uk', 'kbult.ftech.co.uk', 'keith', 'keith', 'keith', 'keith', 'like', 'literature', 'look', 'looks',
        'looks', 'ltd', 'made', 'made', 'mail', 'mailbox', 'main', 'means', 'menu', 'message', 'message', 'message', 'new', 'next',
        'office', 'office', 'one', 'option', 'original', 'outlook', 'outlook', 'pick', 'present', 'product', 'recipients', 'relied',
        'representations', 'research', 'round', 'sales', 'sales', 'search', 'see', 'seein', 'sent', 'server', 'service', 'services',
        'set', 'smtp', 'something', 'soon', 'statements', 'stationary', 'stationary', 'stationary', 'subject', 'subject', 'sunday',
        'sunday', 'sunday', 'sure', 'terms', 'therein', 'time', 'times', 'tomorrow', 'tried', 'try', 'upon', 'upstairs', 'use', 'using',
        'wait', 'waiting', 'way', 'weeks', 'well', 'whether', 'within', 'word', 'world', 'would', 'yup']

    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=Counter(words))
    people.append(doc)

    words = ['1999', 'world', 'bug', 'buddy', 'bye']
    doc = Doc(raw, words=Counter(words))
    people.append(doc)

    words = ['1999', 'world', 'bug', 'band', 'bye']
    doc = Doc(raw, words=Counter(words))
    people.append(doc)


def test_delete_ftx(database):
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Test'
    message['From'] = 'dummy@linux.uk'
    message['To'] = 'dummy@linux.uk'
    html = """<html><body><p>Hello World This is IS a duplicate hello Hello world test</p>
                <div>xxx abc xxx abc xxx xxx</div>
                </body></html>"""
    words = ['1999', '20internet', 'able', 'agreements', 'another', 'anywhere', 'apply', 'back', 'back', 'beyond', 'binding', 'bug', 'bult',
        'bult', 'bult', 'bult', 'bye', 'case', 'cleared', 'cleared', 'click', 'combination', 'come', 'conditions', 'contained',
        'contractually', 'copy', 'couple', 'dad', 'dad', 'dad', 'default', 'descriptions', 'director', 'door', 'else', 'email', 'eve',
        'eve', 'express', 'express', 'format', 'forward', 'frontier', 'frontier', 'frontier', 'ftech', 'ftech.net', 'gareth', 'gareth',
        'gareth', 'gareth', 'get', 'get', 'get', 'get', 'got', 'gottit', 'hoping', 'imceaex', 'instead', 'interesting', 'internet',
        'january', 'kbult.ftech.co.uk', 'kbult.ftech.co.uk', 'keith', 'keith', 'keith', 'keith', 'like', 'literature', 'look', 'looks',
        'looks', 'ltd', 'made', 'made', 'mail', 'mailbox', 'main', 'means', 'menu', 'message', 'message', 'message', 'new', 'next',
        'office', 'office', 'one', 'option', 'original', 'outlook', 'outlook', 'pick', 'present', 'product', 'recipients', 'relied',
        'representations', 'research', 'round', 'sales', 'sales', 'search', 'see', 'seein', 'sent', 'server', 'service', 'services',
        'set', 'smtp', 'something', 'soon', 'statements', 'stationary', 'stationary', 'stationary', 'subject', 'subject', 'sunday',
        'sunday', 'sunday', 'sure', 'terms', 'therein', 'time', 'times', 'tomorrow', 'tried', 'try', 'upon', 'upstairs', 'use', 'using',
        'wait', 'waiting', 'way', 'weeks', 'well', 'whether', 'within', 'word', 'world', 'would', 'yup']

    part2 = MIMEText(html, "html")
    message.attach(part2)
    raw = message.as_string()

    people = database.table('people', codec=SerialiserType.RAW)
    doc = Doc(raw, words=Counter(words))
    people.append(doc)

    index = people['full_text']
    bitmap = Bitmap(index._bitmap)

    with WriteTransaction(database) as txn:
    # with database.env.begin(db=index._bitmap) as txn:
        assert index.ftx_query(['well'], txn=txn)[0] == 1, 'search failed'

    test = b'\x02' + (b'\x00' * (InvertedWordIndex.BITMAP_CHUNK - 1))
    with WriteTransaction(database) as txn:
    # with database.env.begin(db=index._bitmap) as txn:
        cursor = txn.txn.cursor(db=index._bitmap)
        while cursor.next():
            index, count = unpack('>LL', cursor.key())
            page = bitmap.fetch(index,0, txn)
            assert test == page, 'bad page'
    index = people['full_text']

    people.delete(doc)

    test = b'\x00' * InvertedWordIndex.BITMAP_CHUNK
    with WriteTransaction(database) as txn:
    # with database.env.begin(db=index._bitmap) as txn:
        cursor = txn.txn.cursor(db=index._bitmap)
        while cursor.next():
            index, count = unpack('>LL', cursor.key())
            page = bitmap.fetch(index,0, txn)
            assert test == page, 'bad page'
    index = people['full_text']

def test_words (people):
    index = people['full_text']
    with ReadTransaction(people._database) as txn:
        assert index.iwx_counts(txn=txn)['words'] == 2
    key, count = index.dump()['lexicon'][0]
    assert key == '1999'
    assert count == 2

    people.delete(people.first().key)

    with ReadTransaction(people._database) as txn:
        assert index.iwx_counts(txn=txn)['words'] == 1
    key, count = index.dump()['lexicon'][0]
    assert key == '1999'
    assert count == 1


def test_InvertedWordIndex_Lexicon(version):

    with TemporaryDirectory() as dir:
        db_file_name = (Path(dir) / DATABASE).as_posix()
        config = {
            'writemap': False,
            'map_size': 1024 * 1024,
            'auto_resize': True,
            'version': version
        }
        database = Manager().database('db', db_file_name, config=config)
        table = database.table('people')
        table.ensure('full_text', iwx=True)

        iwx = InvertedWordIndex(config={'iwx': True})
        with WriteTransaction(database) as transaction:
            iwx.env = database.env
            iwx.open(transaction)
            words = ['hello', 'world', 'this', 'is', 'a', 'test']
            lexicon = Lexicon(iwx).from_words(b'1', Counter(words), transaction)
            assert str(Counter(words)) == str(lexicon._words), 'read/write mismatch'
            lexicon = Lexicon(iwx).from_oid(b'1', transaction)
            assert str(words) == str(list(lexicon._words)), 'read/write mismatch'
            count = 1
            for word in words:
                index = lexicon.lookup_word(word.encode(), transaction)
                assert index == count, 'bad index'
                count += 1
            assert iwx.iwx_counts(transaction)['lexicon'] == 6, 'bad word count'
            lexicon.update_words(transaction)
            assert iwx.iwx_counts(transaction)['docindx'] == 1, 'bad doc count'
            assert iwx.iwx_counts(transaction)['bitmap'] == 6, 'bad bitmap count'
            assert iwx.iwx_counts(transaction)['words'] == 1, 'bad words count'


def test_InvertedWordIndex_write(database):
    iwx = InvertedWordIndex(config={'iwx': True})
    with WriteTransaction(database) as transaction:
        iwx.env = database.env
        iwx.open(transaction)
        # print(iwx.iwx_counts(transaction.txn))
        words = ['hello', 'world', 'this', 'is', 'a', 'test']
        iwx.write(b'1', Counter(words), transaction)
        assert iwx.iwx_counts(transaction)['lexicon'] == 6, 'bad word count'
        assert iwx.iwx_counts(transaction)['docindx'] == 1, 'bad doc count'
        assert iwx.iwx_counts(transaction)['bitmap'] == 6, 'bad bitmap count'
        assert iwx.iwx_counts(transaction)['words'] == 1, 'bad words count'


def test_InvertedWordIndex_search(database):
    iwx = InvertedWordIndex(config={'iwx': True})
    iwx.env = database.env
    with WriteTransaction(database) as transaction:
        iwx.open(transaction)
        words = ['hello', 'world', 'this', 'is', 'a', 'test']
        iwx.write(b'1', Counter(words), transaction)
        x = iwx.ftx_query(['world'], txn=transaction)
        assert x[0] == 1 and x[1]==[b'1'], 'find failed'
        x = iwx.ftx_query(['text'], txn=transaction)
        assert x[0] == 0 and x[1]==[], 'find should have failed'
        x = iwx.iwx_lexicon(['t'], 20, txn=transaction)
        assert x[0][0] == 'test' and x[0][1] == 1, 'bad lexicon'
        assert x[1][0] == 'this' and x[1][1] == 1, 'bad lexicon'
    # iwx.dump(True)
    with WriteTransaction(database) as transaction:
        lexicon = Lexicon(iwx).from_oid(b'1', transaction)
        lexicon.delete(transaction)
    # iwx.dump(True)
    with WriteTransaction(database) as transaction:
        iwx.open(transaction)
        x = iwx.ftx_query(['world'], txn=transaction)
        assert x[0] == 0 and x[1] == [], 'bad lexicon'

def test_reindex (people):
    index = people['full_text']
    with ReadTransaction(people._database) as txn:
        assert index.iwx_counts(txn=txn)['words'] == 2
    key, count = index.dump()['lexicon'][0]
    assert key == '1999'
    assert count == 2

    # with ReadTransaction(people._database) as txn:
    #     print(index.iwx_counts(txn=txn.txn))

    people.reindex('full_text')

    # with ReadTransaction(people._database) as txn:
    #     print(index.iwx_counts(txn=txn.txn))


    # people.delete(people.first().key)

    # with ReadTransaction(people._database) as txn:
    #     assert index.iwx_counts(txn=txn.txn)['words'] == 1
    # key, count = index.dump()['lexicon'][0]
    # assert key == '1999'
    # assert count == 1

