from unittest import TestCase
from orbit_database import Manager, Database
from shutil import rmtree
import pytest

DATABASE = '.test_database'

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    db.close()


@pytest.fixture
def data ():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]

def test_manager_basic ():
    Manager()


def test_manager_len (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    manager.database(DATABASE, config={'version': version})
    assert len(manager) == 1


def test_manager_get_fail (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    manager.database(DATABASE, config={'version': version})
    assert 'fred' not in manager


def test_manager_close (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    manager.database(DATABASE, config={'version': version})
    manager[DATABASE].close()
    assert len(manager) == 1
    assert [manager[db].isopen for db in manager] == [False]


def test_database (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    db = manager.database(DATABASE, config={'version': version})
    for key, val in manager.items():
        assert key == DATABASE
        assert isinstance(val, Database) == True
    assert 'fred' not in manager
    db.close()
    assert [manager[db].isopen for db in manager] == [False]
    assert len(manager) == 1
    db = manager.database(DATABASE, config={'version': version})
    assert [manager[db].isopen for db in manager] == [True]


def test_database_configure (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    config = {
        'map_size': 1024 * 1024 * 10,
        'version': version
    }
    db = manager.database(DATABASE, config=config)
    assert db.map_size == 1024 * 1024 * 10
    db.close()
    db = manager.database(DATABASE)
    assert db.map_size == 1024 * 1024 * 10

def test_manager_db_repr (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    db = manager.database(DATABASE, config={'version': version})
    assert str(db) == '<orbit_database.database.Database instance> path=".test_database" status=open'

def test_manager_db_sync(db):
    db.sync(False)
    db.sync(True)

def test_double_open(version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    db1 = manager.database(DATABASE, config={'version': version})
    db2 = manager.database(DATABASE, config={'version': version})
    assert db1 == db2


def test_double_close (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()   
    db = manager.database(DATABASE, config={'version': version})
    db.close()
    assert db.isopen == False


#def test_storage_used (version):
#    rmtree(DATABASE, ignore_errors=True)
#    manager = Manager()   
#    db = manager.database(DATABASE, config={'version': version})
#    storage = db.storage_used()
#    if version == 1:
#        #assert storage[0] == 20480
#        assert storage[1] == {'__audit_table__': 8192, '__metadata__': 12288}
#        assert db.storage_allocated == 10485760
#    else:
#        #assert storage[0] == 28672
#        assert storage[1] == {'@@catalog@@': 8192, '@@audit_table@@': 8192, '@@metadata@@': 12288}
#        assert db.storage_allocated == 10485760


def test_manager_setget (version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()   
    manager.database(DATABASE, config={'version': version})
    assert DATABASE in manager
    assert DATABASE in manager.keys()

def test_manager_items (version):
    manager = Manager()
    manager.database(DATABASE, config={'version': version})
    for key, val in manager.items():
        assert key == DATABASE
        assert isinstance(val, Database)

# FIXME: !!!

# def test_manager_reopen (version):
#     manager = Manager()
#     manager.database(DATABASE, config={'version': version})
#     assert len(manager) == 1
#     manager[DATABASE].close()
    # manager.database(DATABASE, config={'version': version})
    # assert len(manager) == 1
