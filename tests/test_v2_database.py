#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc, CompressionType, SerialiserType
from shutil import rmtree
import pytest

DATABASE = '.test_database'


@pytest.fixture
def db():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': 1})
    yield db
    db.close()


def test_old_database_structure (db):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': 1})
    table = db.table('TestTable')
    table.append(Doc({'text': 'Hello World'}))
    assert list(db.tables()) == ['TestTable']
    table.ensure('by_text', '{text}')
    assert list(table.indexes()) == ['by_text'], 'index issue'
    have = list(db.tables(all=True)) 
    want = ['TestTable', '_TestTable_by_text', '__audit_table__', '__metadata__']
    # print()
    # print(have)
    # print(want)
    assert have == want, 'issue with V1 structure'


def test_new_database_structure (db):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': 2})
    table = db.table('TestTable')
    table.append(Doc({'text': 'Hello World'}))
    assert list(db.tables()) == ['TestTable']
    table.ensure('by_text', '{text}')
    assert list(table.indexes()) == ['by_text'], 'index issue'
    have = set(list(db.tables(all=True)))
    want = set(['@@catalog@@', 'TestTable', '@@metadata@@', '@@audit_table@@'])
    assert have == want, 'issue with V2 structure'


def test_drop_old_database_index (db):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': 1})
    table = db.table('TestTable')
    table.append(Doc({'text': 'Hello World'}))
    assert list(db.tables()) == ['TestTable']
    table.ensure('by_text', '{text}')
    assert list(table.indexes()) == ['by_text'], 'index issue'
    table.drop('by_text')

    have = list(db.tables(all=True)) 
    want = ['TestTable', '__audit_table__', '__metadata__']
    assert have == want, 'issue with V1 drop'
    

def test_drop_new_database_index (db):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': 2})
    table = db.table('TestTable')
    table.append(Doc({'text': 'Hello World'}))
    assert list(db.tables()) == ['TestTable']
    table.ensure('by_text', '{text}')

    assert list(table.indexes()) == ['by_text'], 'index issue'
    table.drop('by_text')

#     have = set(list(db.tables(all=True)))
#     want = set(['@@catalog@@', 'TestTable', '@@metadata@@', '@@audit_table@@'])

#     print()
#     print(">>", list(db.indexes()))

    # assert have == want, 'issue with V2 structure'
    # assert list(table.indexes()) == []
    
