#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
import pickle
import time
import pytest

from orbit_database import ObjectId, InvalidId


def test_generation_time():
    time_before = time.time()
    oid = ObjectId()
    time_after = time.time()
    assert time_before <= oid.generation_time <= time_after


def test_binary():
    oid = ObjectId()
    oid_copy = ObjectId(oid.binary)
    assert oid == oid_copy
    assert oid.binary == oid_copy.binary


def test_pickle():
    oid = ObjectId()
    assert pickle.loads(pickle.dumps(oid)) == oid


def test_str():
    oid = ObjectId()
    oid_str = str(oid)
    assert isinstance(oid_str, str)
    assert len(oid_str) == 32


def test_make_from_string():
    oid = ObjectId()
    new = ObjectId(str(oid))
    assert new == oid


def test_ordering():
    oid0 = ObjectId()
    oid1 = ObjectId()
    assert oid0 < oid1
    assert oid0 <= oid1
    assert oid1 > oid0
    assert oid1 >= oid0
    assert oid0 != oid1
    assert oid0 == oid0
    assert oid0 == ObjectId(oid0.binary)


def test_hash():
    oid0 = ObjectId()
    oid1 = ObjectId()
    assert isinstance(hash(oid0), int)
    assert hash(oid0) != hash(oid1)


def test_invalid():
    with pytest.raises(InvalidId):
        ObjectId('crap')

