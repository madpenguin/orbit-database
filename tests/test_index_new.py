#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc
from shutil import rmtree
from time import sleep
import pytest

DATABASE = '.database'
TABLE_NAME = 'people'


@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db


@pytest.fixture
def data():
    return [
        {'folder': 'AAA', 'name': 'TomA', 'age': 20},
        {'folder': 'BBB', 'name': 'DickB', 'age': 30},
        {'folder': 'AAA', 'name': 'HarryA', 'age': 25},
        {'folder': 'BBB', 'name': 'SamB', 'age': 35},
        {'folder': 'AAA', 'name': 'SallyA', 'age': 22},
        {'folder': 'BBB', 'name': 'SallyB', 'age': 25},
        {'folder': 'CCC', 'name': 'SallyC', 'age': 21},
        {'folder': 'DDD', 'name': 'SallyD', 'age': 21},
        {'folder': 'AAA', 'name': 'SallyX', 'age': 29},
        {'folder': 'BBB', 'name': 'SallyF', 'age': 21},
    ]


@pytest.fixture
def people(db, data):
    result = db[TABLE_NAME].open()
    result.ensure('by_folder', '{folder}', duplicates=True)
    with db[TABLE_NAME].write_transaction as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result


def test_ensure_change_detection_works(people):
    keys = [person.doc._folder for person in people.filter('by_folder')]
    assert keys[0] == 'AAA', 'bad order'
    assert keys[-1] == 'DDD', 'bad order'
    assert getattr(people['by_folder'], 'reindexed'), 'index should have been created, wasnt'
    # db = people._database
    # tb = db.table('@@catalog@@')
    # for result in tb.filter():
    #     print(result.doc.doc)
    people.ensure('by_folder', '{folder}', duplicates=True)
    assert not getattr(people['by_folder'], 'reindexed'), 'index should have been left alone, wasnt'
    people.ensure('by_folder', '{name}', duplicates=True)
    assert getattr(people['by_folder'], 'reindexed'), 'index should have been reindexed, wasnt'
    keys = [person.doc._name for person in people.filter('by_folder')]
    assert keys[0] == 'DickB', 'bad order'
    assert keys[-1] == 'TomA', 'bad order'

