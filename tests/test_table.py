#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc, CompressionType, WriteTransaction, NoSuchTable, \
    NoSuchIndex, InvalidKeySpecifier, SerialiserType
from datetime import datetime
from lmdb import ReadonlyError
from shutil import rmtree
import pytest


DATABASE = '.database'
COMPRESSIONS = [(CompressionType.ZSTD, 5), (CompressionType.SNAPPY, None), (CompressionType.NONE, None)]


@pytest.fixture(params=[SerialiserType.UJSON, SerialiserType.ORJSON, SerialiserType.NONE])
def codec(request):
    return request.param


@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]


@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    db.close()


@pytest.fixture
def people_table(request, data, db):
    result = db['peoplex'].open()
    with WriteTransaction(db) as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result

@pytest.fixture(params=COMPRESSIONS)
def people(request, data, db, codec):
    compression_type = request.param[0]
    compression_level = request.param[1]
    result = db['peoplex'].open(compression_type=compression_type, compression_level=compression_level, codec=codec)
    with WriteTransaction(db) as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result


def test_append(data, people):
    for person in people.find():
        item = data.pop(0)
        del item['_id']


def test_get(data, people):
    for person in data:
        doc = people.get(person['_id'])
        del person['_id']
        assert doc.doc == person, 'Recovered record does not match written'


def test_put(data, people):
    doc = Doc({'text': 'Junk'})
    with people.write_transaction as txn:
        doc.put(people, append=False, txn=txn.txn)
    xxx = people.get(doc.key)
    assert xxx.doc == doc.doc, 'Recovered record does not match written'
    with people._database.write_transaction as txn:
        doc.put(people, append=False, txn=txn.txn)


def test_put_fail(data, people):
    doc = Doc({'text': 'Junk'})
    with pytest.raises(Exception):
        with people.read_transaction as txn:
            doc.put(None, append=False, txn=txn.txn)


def test_put_fail_2(data, people):
    doc = Doc({'text': 'Junk'})
    with pytest.raises(ReadonlyError):
        with people.read_transaction as txn:
            doc.put(people, append=False, txn=txn.txn)


def test_get_none(data, people):
    doc = people.get('none')
    assert doc is None, f'Expecting a None value, received {doc}'


def test_get_with_transaction(data, people):
    with people.read_transaction as txn:
        for person in data:
            doc = people.get(person['_id'], txn=txn)
            del person['_id']
            assert doc.doc == person, 'Recovered record does not match written'


def test_save(data, people):
    for person in people.find():
        person._name += "!"
        person._dob = datetime.now().timestamp()
        people.save(person)
    for person in people.find():
        assert person._dob is not None, 'Recovered record does not match written'


def test_empty(data, people):
    assert people.records() == 5, 'Wrong number of records in table'
    people.empty()
    assert people.records() == 0, 'Wrong number of records in table'


def test_tail_half(data, people):
    oid = data[1]['_id']
    assert [doc._name for doc in people.tail(oid)] == ['Harry', 'Sam', 'Sally'], (
        '"tail" Acquired the wrong records')


def test_tail_all(data, people):
    assert [doc._name for doc in people.tail()] == ['Tom', 'Dick', 'Harry', 'Sam', 'Sally'], (
        '"tail" Acquired the wrong records')


def test_tail_last(data, people):
    oid = data[-2]['_id']
    assert [doc._name for doc in people.tail(oid)] == ['Sally'], (
        '"tail" Acquired the wrong records')


def test_tail_none(data, people):
    oid = 'crap'
    required = ['Tom', 'Dick', 'Harry', 'Sam', 'Sally']
    assert [doc._name for doc in people.tail(oid)] == required, (
        '"tail" Acquired the wrong records')


def test_tail_none_empty(data, people):
    oid = 'crap'
    people.empty()
    assert [doc._name for doc in people.tail(oid)] == [], (
        '"tail" Acquired the wrong records')


def test_tail_no_new(data, people):
    oid = data[-1]['_id']
    assert [doc._name for doc in people.tail(oid)] == [], (
        '"tail" Acquired the wrong records')


def test_first(data, people):
    assert people.first()._name == 'Tom', '"first" acquired wrong record'
    people.empty()
    assert people.first() is None, '"first" acquired wrong record'
    assert people.last() is None, '"last" acquired wrong record'
    with pytest.raises(NoSuchIndex):
        people.first('no_such_index')


def test_last(data, people):
    assert people.last()._name == 'Sally', '"last" acquired wrong record'
    with pytest.raises(NoSuchIndex):
        people.last('no_such_index')


def test_last_with_db_read_txn(data, people):
    db = people._database
    with db.read_transaction as txn:
        assert people.last(txn=txn)._name == 'Sally', '"last" acquired wrong record'


def test_range_all_incl(data, people):
    lower = Doc(data[0], data[0]['_id'])
    upper = Doc(data[-1], data[-1]['_id'])
    assert [doc._name for doc in people.range(lower=lower, upper=upper)] == ['Tom', 'Dick', 'Harry', 'Sam', 'Sally'], (
        '"range" returned wrong data for inclusive set')


def test_range_all(data, people):
    lower = Doc(data[0], data[0]['_id'])
    upper = Doc(data[-1], data[-1]['_id'])
    assert [doc._name for doc in people.range(lower=lower, upper=upper, inclusive=False)] == ['Dick', 'Harry', 'Sam'], (
        '"range" returned wrong data for exclusive set')


def test_range_no_lower_incl(data, people):
    upper = Doc(data[-1], data[-1]['_id'])
    assert [doc._name for doc in people.range(upper=upper)] == ['Tom', 'Dick', 'Harry', 'Sam', 'Sally'], (
        '"range" returned wrong data for inclusive set')


def test_range_no_lower(data, people):
    upper = Doc(data[-1], data[-1]['_id'])
    assert [doc._name for doc in people.range(upper=upper, inclusive=False)] == ['Tom', 'Dick', 'Harry', 'Sam'], (
        '"range" returned wrong data for no lower')


def test_range_no_upper(data, people):
    lower = Doc(data[0], data[0]['_id'])
    assert [doc._name for doc in people.range(lower=lower, inclusive=False)] == ['Dick', 'Harry', 'Sam', 'Sally'], (
        '"range" returned wrong data for no upper')


def test_range_no_upper_excl(data, people):
    lower = Doc(data[0], data[0]['_id'])
    required = ['Tom', 'Dick', 'Harry', 'Sam', 'Sally']
    assert [doc._name for doc in people.range(lower=lower, inclusive=True)] == required, (
        '"range" returned wrong data for no lower excl')


def test_range_no_range_excl(data, people):
    assert [doc._name for doc in people.range(inclusive=True)] == ['Tom', 'Dick', 'Harry', 'Sam', 'Sally'], (
        '"range" returned wrong data for no range')


def test_range_keyonly(data, people):
    assert [key for key in people.range(keyonly=True)] == [d['_id'] for d in data], (
        "range keyonly produced wrong keys")


def test_drop_table(data, people):
    db = people._database
    version = db.index_version
    if version == 1:
        want = set(['__audit_table__', '__metadata__', 'peoplex'])
    else:
        want = set(['@@audit_table@@', '@@metadata@@', 'peoplex'])
    db.drop('peoplex')
    t1 = db.table('test1')
    t2 = db.table('test2')
    t1.append(Doc({'name': 't1'}))
    t2.append(Doc({'name': 't2'}))
    if version == 1:
        want = set(['__audit_table__', '__metadata__', 'test1', 'test2'])
    else:
        want = set(['@@audit_table@@', '@@metadata@@', 'test1', 'test2'])
    have = set(db.keys())
    assert have == want


def test_drop_no_such_table(data, people):
    db = people._database
    with pytest.raises(NoSuchTable):
        db.drop('no such table')


def test_find_no_index(data, people):
    with pytest.raises(NoSuchIndex):
        list(people.find('no index'))


def test_find_expression(data, people):
    assert list(people.find(expression=lambda doc: doc["name"] == "Harry"))[0]._name == 'Harry', (
        'find based on expression failed')


def test_delete(data, people):
    people.delete(data[0]['_id'])
    people.delete([data[1]['_id'], ])
    key = data[2]['_id'].encode()
    people.delete(key)
    doc = next(people.find())
    people.delete(doc)
    assert people.records() == 1, 'wrong number of records left after deletes'
    with pytest.raises(InvalidKeySpecifier):
        people.delete({})


def test_range_limit_page_number(data, people):
    required = ['Tom', 'Dick', 'Harry', 'Sam', 'Sally']
    for r, l in ((5, 1), (3, 2), (2, 3), (2, 4), (1, 5)):
        lookfor = list(doc._name for i in range(r) for doc in people.range(limit=l, page_number=i))
        assert lookfor == required, '"range" with limit and page_number returned wrong data'


#def test_storage_used(people):
#    assert people.storage_used() == 12288


