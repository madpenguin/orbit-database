#####################################################################################
#
#  Copyright (c) 2021 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc, CompressionType, WriteTransaction, SerialiserType
from shutil import rmtree
import pytest

DATABASE = '.database'
COMPRESSIONS = [(CompressionType.ZSTD, 5), (CompressionType.SNAPPY, None), (CompressionType.NONE, None)]


@pytest.fixture(params=[SerialiserType.UJSON, SerialiserType.ORJSON, SerialiserType.NONE])
def codec(request):
    return request.param

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def data():
    return [
        {'folder': 'AAA', 'name': 'TomA', 'age': 20},
        {'folder': 'BBB', 'name': 'DickB', 'age': 30},
        {'folder': 'AAA', 'name': 'HarryA', 'age': 25},
        {'folder': 'BBB', 'name': 'SamB', 'age': 35},
        {'folder': 'AAA', 'name': 'SallyA', 'age': 22},
        {'folder': 'BBB', 'name': 'SallyB', 'age': 25},
        {'folder': 'CCC', 'name': 'SallyC', 'age': 21},
        {'folder': 'DDD', 'name': 'SallyD', 'age': 21},
        {'folder': 'AAA', 'name': 'SallyX', 'age': 29},
        {'folder': 'BBB', 'name': 'SallyF', 'age': 21},
    ]


@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    db.close()


@pytest.fixture(params=COMPRESSIONS)
def people(request, data, db, codec):
    compression_type = request.param[0]
    compression_level = request.param[1]
    result = db['peoplex'].open(compression_type=compression_type, compression_level=compression_level, codec=codec)
    result.ensure('by_folder', '{folder}', duplicates=True)
    with WriteTransaction(db) as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result


def test_basic(data, people):
    assert len(list(people.filter())) == 10, 'wrote wrong number of records'


def test_first(data, people):
    assert people.first()._name == 'TomA', '"first" acquired wrong record'
    assert people.first('by_folder')._folder == 'AAA', '"first" by _folder acquired wrong record'
    assert people.first('by_folder', Doc({'folder': 'BBB'}))._name == 'DickB', '"first" by _folder acquired wrong record'
    assert people.first('by_folder', Doc({'folder': 'CCC'}))._name == 'SallyC', '"first" by _folder acquired wrong record'
    assert people.first('by_folder', Doc({'folder': 'DDD'}))._name == 'SallyD', '"first" by _folder acquired wrong record'


def test_last(data, people):
    assert people.last()._name == 'SallyF', '"last" acquired wrong record'
    assert people.last('by_folder')._folder == 'DDD', '"last" by _folder acquired wrong record'
    assert people.last('by_folder', Doc({'folder': 'AAA'}))._name == 'SallyX', '"last" by _folder acquired wrong record'
    assert people.last('by_folder', Doc({'folder': 'BBB'}))._name == 'SallyF', '"last" by _folder acquired wrong record'
    assert people.last('by_folder', Doc({'folder': 'CCC'}))._name == 'SallyC', '"last" by _folder acquired wrong record'
    assert people.last('by_folder', Doc({'folder': 'DDD'}))._name == 'SallyD', '"last" by _folder acquired wrong record'


def test_append(data, people):
    people.append(Doc({'folder': 'DDD', 'name': 'SallyA'}))
    assert people.last('by_folder', Doc({'folder': 'DDD'}))._name == 'SallyA', '"last" by _folder acquired wrong record'
    people.append(Doc({'folder': 'DDD', 'name': 'SallyZ'}))
    assert people.last('by_folder', Doc({'folder': 'DDD'}))._name == 'SallyZ', '"last" by _folder acquired wrong record'
