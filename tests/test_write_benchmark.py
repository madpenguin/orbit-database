#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc, CompressionType, SerialiserType, WriteTransaction
from os import urandom
from base64 import b64encode
from time import time
from tqdm import tqdm
import pytest
from shutil import rmtree


DATABASE = '.test_database'
COMPRESSIONS = [(CompressionType.SNAPPY, None), (CompressionType.NONE, None)]
RECORD_SIZES = [2**n for n in range(24)]

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture(params=[SerialiserType.UJSON, SerialiserType.ORJSON, SerialiserType.NONE])
def codec(request):
    return request.param


@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'map_size': 1024*1024*1024*64, 'version': version})
    yield db
    db.close()


@pytest.fixture(params=RECORD_SIZES)
def record_size(request):
    return request.param


@pytest.fixture(params=COMPRESSIONS)
def compression(request):
    return request.param


def test_write(db, record_size):
    record_count = 10
    table = db.table('messages')

    s = b64encode(urandom(record_size)).decode('utf-8')
    doc = Doc({'random': s })
    beg = time()
    with WriteTransaction(db) as txn:
        for n in tqdm(range(record_count)):
            table.append(doc, txn=txn)
    end = time()
    dif = end-beg
    print(f'Records={table.records()} size={record_size} Time={dif:.4}')
