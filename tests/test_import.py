from orbit_database import Manager, Doc
from shutil import rmtree
from pathlib import Path
import pytest

DATABASE = '.test_database'
JSONFILE = '.test_people.json'

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    db.close()

@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]


def test_import(db):
    table = db.table('mimedb')
    records = table.import_from_file('tests/data/mimedb.json')
    print("Imported: ", records)
    assert records == table.records(), 'record count is incorrect'

def test_export(db, data):
    table = db.table('people')
    for person in data:
        table.append(Doc(person))
    Path(JSONFILE).unlink(missing_ok=True)
    table.export_to_file(JSONFILE)

def test_save_load(db, data):
    table = db.table('people')
    for person in data:
        table.append(Doc(person))
    Path(JSONFILE).unlink(missing_ok=True)
    table.export_to_file(JSONFILE)
    new_table = db.table('import')
    records = new_table.import_from_file(JSONFILE)
    assert records == new_table.records(), 'record count is incorrect'
    assert records == table.records(), 'record count is incorrect'


    # people_table.export_to_file('/tmp/people.export', True)
    # imp = db['people_imp'].open()
    # imp.import_from_file('/tmp/people.export')
    # iter = imp.filter()
    # count = 0
    # for result in people_table.filter():
    #     doc1 = result.doc.doc
    #     doc2 = next(iter).doc.doc
    #     assert doc1 == doc2, 'import and export not the same'
    #     count += 1
    # assert count == 5, 'wrong number of records imported'
