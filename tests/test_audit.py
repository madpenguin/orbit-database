#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc
from shutil import rmtree
from asyncio import sleep
import pytest

DATABASE = '.database'
DELAY = 0.01

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
async def make_database(version):
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    db = manager.database(DATABASE, config={'auditing': True, 'version': version})
    yield db
    manager.close()


@pytest.mark.asyncio
async def test_open_database(make_database):
    async for database in make_database:
        assert database.auditor.uuid != None, 'UUID check failed'

@pytest.mark.asyncio
async def test_append_record(make_database):   

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in make_database:
        results = []
        people = database.table('people', auditing=True, callback=monitor)
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Tom'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        await sleep(DELAY)
        assert len(results) == 4


@pytest.mark.asyncio
async def test_audit_changes(make_database):

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in make_database:
        results = []
        people = database.table('people', auditing=True, callback=monitor)
        doc = Doc({'name': 'Fred', 'age': 21, 'height': 6, 'tags': [1,2,3]})
        people.append(doc)
        await sleep(DELAY)

        assert len(results) == 1
        for doc in results:
            assert doc._e == 1

        doc = people.get(doc._o)
        doc._name = "FredNew"
        people.save(doc)
        await sleep(DELAY)

        assert len(results) == 2
        for doc in results[1:]:
            assert doc._e == 2
            assert doc._c == {'name': 'Fred', 'age': 21, 'height': 6, 'tags': [1, 2, 3]}

        doc = people.get(doc._o)
        doc._tags = [1,2]
        people.save(doc)
        await sleep(DELAY)

        assert len(results) == 3
        for doc in results[2:]:
            assert doc._e == 2
            assert doc._c == {'name': 'FredNew', 'age': 21, 'height': 6, 'tags': [1, 2, 3]}


@pytest.mark.asyncio
async def test_audit_enable_disable(make_database):

    async def monitor(docs):
        nonlocal results
        results += docs
    
    async for database in make_database:
        results = []
        people = database.table('people')
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Tom'}))

        people.reopen(auditing=True, callback=monitor)
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        assert people.records() == 4
        await sleep(DELAY)
        assert len(results) == 2

        people.reopen()
        people.append(Doc({'name': 'AAA'}))
        people.append(Doc({'name': 'BBB'}))
        assert people.records() == 6
        await sleep(DELAY)
        assert len(results) == 2

@pytest.mark.asyncio
async def test_audit_delete(make_database):

    async def monitor(docs):
        nonlocal results
        results += docs
    
    async for database in make_database:
        results = []
        people = database.table('people', auditing=True, callback=monitor)
        doc = people.append(Doc({'name': 'John'}))
        people.delete(doc)
        await sleep(DELAY)
        assert people.records() == 0
        for doc in results:
            if doc._e == 3:
                assert doc._c == {'name': 'John'}

@pytest.mark.asyncio
async def test_audit_callback(make_database):
    
    async def KickMe(docs):
        nonlocal results
        results += docs

    async def KickMe2(docs):
        nonlocal results
        results += docs

    async for database in make_database:
        results = []
        people = database.table('people', auditing=True, callback=KickMe)
        people.watch(KickMe2)
        people.append(Doc({'name': 'John'}))
        people.append(Doc({'name': 'Tom'}))
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        await sleep(DELAY)
        assert len(results) == 18

@pytest.mark.asyncio
async def test_audit_reopen_callback(make_database):
    
    async def KickMe(docs):
        nonlocal results
        results += docs

    async for database in make_database:
        results = []
        people = database.table('people')
        people.append(Doc({'name': 'John'}))
        people.append(Doc({'name': 'Tom'}))
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        people.reopen(auditing=True, callback=KickMe)
        people.append(Doc({'name': 'XXX'}))
        await sleep(DELAY)
        assert people.records () == 10
        assert len(results) == 1

@pytest.mark.asyncio
async def test_default_handler(make_database):   

    async for database in make_database:
        results = []
        people = database.table('people', auditing=True)
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Tom'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        await sleep(DELAY)

@pytest.mark.asyncio
async def test_watch_unwatch(make_database):   

    async def monitor1(docs):
        nonlocal results1
        results1 += docs

    async def monitor2(docs):
        nonlocal results2
        results2 += docs

    async for database in make_database:
        results1 = []
        results2 = []
        people = database.table('people', auditing=True)
        people.watch(monitor1)
        people.watch(monitor2)
        people.append(Doc({'name': 'Fred'}))
        people.append(Doc({'name': 'Tom'}))
        people.append(Doc({'name': 'Dick'}))
        people.append(Doc({'name': 'Harry'}))
        await sleep(DELAY)
        assert len(results1) == 4
        assert len(results2) == 4
        
        people.unwatch(monitor1)
        people.append(Doc({'name': 'Harry'}))
        await sleep(DELAY)
        assert len(results1) == 4
        assert len(results2) == 5
