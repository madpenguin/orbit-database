from unittest import TestCase
from orbit_database import Doc


class UnitTests(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_doc(self):
        doc = Doc()
        self.assertEqual(doc.oid, None)
        self.assertEqual(doc.doc, {})
        self.assertEqual(doc.upd, {})

    def test_doc_init(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(doc.key, 'myoid')
        self.assertEqual(doc.upd, {'name': 'Fred'})
        self.assertEqual(doc.dat, {})
        self.assertEqual(doc.doc, {'name': 'Fred'})

    def test_doc_attr(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(doc._name, 'Fred')
        self.assertEqual(doc['name'], 'Fred')
        doc._age = 24
        self.assertEqual(doc._age, 24)
        self.assertEqual(doc['age'], 24)
        self.assertEqual(doc.doc, {"name": "Fred","age":24})
        del doc['age']
        self.assertEqual(doc._age, None)
        self.assertEqual(doc['age'], None)
        self.assertEqual(doc.doc, {"name":"Fred"})
        del doc['name']
        self.assertEqual(doc._name, None)
        self.assertEqual(doc['name'], None)
        self.assertEqual(doc.doc, {})
        doc['name'] = 'Tom'
        self.assertEqual(doc._name, 'Tom')
        self.assertEqual(doc['name'], 'Tom')

    def test_doc_update(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(doc.key, 'myoid')
        self.assertEqual(doc.upd, {'name': 'Fred'})
        self.assertEqual(doc._name, 'Fred')
        self.assertEqual(len(doc), 1)
        self.assertEqual(doc.doc, {"name": "Fred"})
        doc._name = 'Jim'
        self.assertEqual(doc.dat, {})
        self.assertEqual(doc.doc, {'name': 'Jim'})
        self.assertEqual(doc._name, 'Jim')
        self.assertEqual(len(doc), 1)
        self.assertEqual(doc.doc, {"name":"Jim"})

    def test_doc_changed(self):
        doc = Doc({}, 'myoid')
        self.assertFalse(doc.changed)
        doc._age = 24
        self.assertTrue(doc.changed)

    def test_doc_get(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        with self.assertRaises(AttributeError):
            print(doc.none)

    def test_doc_repr(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(str(doc), "<class 'orbit_database.doc.Doc'> with id=b'myoid'")

    def test_doc_del(self):
        doc = Doc()
        doc.dat = {'name': 'Fred', 'age': 24}
        self.assertEqual(doc.dat, {'name': 'Fred', 'age': 24})
        self.assertEqual(doc.rem, [])
        del doc['name']
        self.assertEqual(doc.dat, {'age': 24})
        self.assertEqual(doc.rem, ['name'])

    def test_keys(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(list(doc.keys()), ['name'])

    def test_items(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(list(doc.items()), [('name', 'Fred')])

    def test_values(self):
        doc = Doc({'name': 'Fred'}, 'myoid')
        self.assertEqual(list(doc.values()), ['Fred'])

    def test_pop(self):
        doc = Doc({'name': 'Fred', 'popoff': True}, 'myoid')
        self.assertEqual(list(doc.values()), ['Fred', True])
        doc.pop('popoff')
        self.assertEqual(list(doc.values()), ['Fred'])

    def test_equal(self):
        doc1 = Doc({'name': 'Fred', 'popoff': True}, 'myoid')
        doc2 = Doc({'name': 'Fred', 'popoff': True}, 'myoid')
        assert doc1 == doc2, 'equality test failed'
        doc2 = Doc({'name': 'Fred', 'popoff1': True}, 'myoid')
        assert doc1 != doc2, 'inequality test failed'

    def test_doc_with_id(self):
        doc1 = Doc({'name': 'Fred', 'popoff': True}, 'myoid')
        assert doc1.doc_with_id == {'name': 'Fred', 'popoff': True, '_id': 'myoid'}
