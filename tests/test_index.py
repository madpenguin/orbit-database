from orbit_database import Manager, Doc, DuplicateKey, NoSuchIndex, \
    DocumentDoesntExist, IndexAlreadyExists, Index
from tqdm import tqdm
from shutil import rmtree
import pytest

DATABASE = '.test_database'

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    db.close()


@pytest.fixture
def data ():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]

@pytest.fixture
def data2 ():
    return [
        {'name': 'Tom', 'age': 20, 'tags': ['red', 'green', 'blue', 'yellow', 'cyan']},
        {'name': 'Dick', 'age': 30, 'tags': ['red', 'green', 'blue']},
        {'name': 'Harry', 'age': 25, 'tags': ['yellow', 'cyan']},
        {'name': 'Sam', 'age': 35, 'tags': ['red', 'blue', 'yellow']},
        {'name': 'Sally', 'age': 21, 'tags': ['green', 'blue', 'purple']},
    ]


def load_people (table, data):
    for person in data:
        doc = Doc(person)
        table.append(doc)
        person['_id'] = doc.key


@pytest.fixture
def people (db, data):
    table = db.table('people')
    table.ensure('by_name', '{name}', duplicates=True)
    table.ensure('by_age', '{age}', duplicates=True)
    load_people(table, data)
    assert table['by_name'].records() == 5
    assert table['by_age'].records() == 5
    return table


@pytest.fixture
def people2 (db, data2):
    table = db.table('people')
    table.ensure('by_name', '{name}', duplicates=True)
    table.ensure('by_age', '{age}', duplicates=True)
    for person in data2:
        doc = Doc(person)
        table.append(doc)
        person['_id'] = doc.key
    assert table['by_name'].records() == 5
    assert table['by_age'].records() == 5
    return table


def test_ensure_basic ():
    rmtree(DATABASE, ignore_errors=True)
    manager = Manager()
    db = manager.database(DATABASE, config={'version': 1})
    people = db.table('people')
    people.ensure('by_name', '{name}')
    assert list(manager[DATABASE]['people']) == ['by_name']
    assert list(manager[DATABASE]['people'].indexes()) == ['by_name']
    db.close()
    db = manager.database(DATABASE, config={'version': 1})
    assert list(manager[DATABASE]['people'].indexes()) == ['by_name']


def test_range_all_incl(people, data):
    lower = Doc(data[1])
    upper = Doc(data[0])
    have = [doc._name for doc in people.range('by_name', lower=lower, upper=upper)]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want


def test_ensure_index (people, data):
    have = [p._name for p in people.find('by_name')]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'names are wrong'
    have = [p._age for p in people.find('by_age')]
    want = [20, 21, 25, 30, 35]
    assert have == want, 'ages are wrong'


def test_delete(people, data):
    people.delete(data[0]['_id'])
    names = [p._name for p in people.find()]
    names.sort()

    assert names == ['Dick', 'Harry', 'Sally', 'Sam'], 'delete failed'
    assert [p._name for p in people.find('by_name')] == ['Dick', 'Harry', 'Sally', 'Sam']
    assert [p._age for p in people.find('by_age')] == [21, 25, 30, 35]


def test_save(people):
    names = [p._name for p in people.find()]
    names.sort()
    assert names == ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert [p._name for p in people.find('by_name')] == ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert [p._age for p in people.find('by_age')] == [20, 21, 25, 30, 35]

    for person in people.find():
        person._name = person._name[::-1]
        people.save(person)

    assert [p._name for p in people.find('by_name')] == ['kciD', 'maS', 'moT', 'yllaS', 'yrraH']
    assert [p._age for p in people.find('by_age')] == [20, 21, 25, 30, 35]

def test_range_all_incl(people, data):
    lower = Doc(data[1])
    upper = Doc(data[0])
    have = [doc._name for doc in people.range('by_name', lower=lower, upper=upper)]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'test_range failed'


def test_range_all_incl_with_transaction(people, data):
    lower = Doc(data[1])
    upper = Doc(data[0])
    with people.read_transaction as txn:
        have = [doc._name for doc in people.range('by_name', lower=lower, upper=upper, txn=txn)]
        want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        assert have == want, 'test_range failed'


def test_range_all (people, data):
    lower = Doc(data[1])
    upper = Doc(data[0])
    have = [doc._name for doc in people.range('by_name', lower=lower, upper=upper, inclusive=False)]
    want = ['Harry', 'Sally', 'Sam']
    assert have == want, 'range all failed'


def test_range_no_lower_incl (people, data):
    upper = Doc(data[0])
    have = [doc._name for doc in people.range('by_name', upper=upper)]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'no lower failed'


def test_range_no_lower (people, data):
    upper = Doc(data[0])
    have = [doc._name for doc in people.range('by_name', upper=upper, inclusive=False)]
    want = ['Dick', 'Harry', 'Sally', 'Sam']
    assert have == want, 'no lower failed'


def test_range_no_upper (people, data):
    lower = Doc(data[1])
    have = [doc._name for doc in people.range('by_name', lower=lower, inclusive=False)]
    want = ['Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'no upper failed'


def test_range_no_upper_excl (people, data):
    lower = Doc(data[1])
    have = [doc._name for doc in people.range('by_name', lower=lower, inclusive=True)]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'no upper failed'

def test_range_no_range_excl (people, data):
    have = [doc._name for doc in people.range('by_name', inclusive=False)]
    want = ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert have == want, 'no range failed'


def test_range_keyonly (people, data):
    sorted_data = sorted(data, key=lambda doc: doc['name'])
    have = [cursor.val for cursor in people.range('by_name', keyonly=True)]
    want = [item['_id'] for item in sorted_data]
    assert have == want, 'range keyonly failed'


def test_seek_one (people, data):
    del data[0]['_id']
    assert people.seek_one('by_name', Doc({'name': 'Tom'})).doc == data[0]


def test_seek_name (people, data):
    del data[0]['_id']
    del data[3]['_id']
    assert [doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))] == [data[0]]
    assert [doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))] == [data[3]]


def test_seek_age (people, data):
    for item in data:
        del item['_id']
    assert [doc.doc for doc in people.seek('by_age', Doc({'age': 20}), limit=1)] == [data[0]]


def test_seek_name_dups (people, data):
    for p in data:
        del p['_id']
    people.ensure('by_age', '{age}', duplicates=True, force=True)
    for person in data:
        doc = Doc(person)
        people.append(doc)
        person['_id'] = doc.key
    del data[0]['_id']
    del data[3]['_id']
    have = [doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))]
    want = [data[0], data[0]]
    assert have == want    
    have = [doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))]
    want = [data[3], data[3]]
    assert have == want    

def test_drop_table (db, people):
    version = db.index_version
    assert list(db.tables()) == ['people']
    t1 = db.table('test1')
    t2 = db.table('test2')
    t1.append(Doc({'name': 't1'}))
    t2.append(Doc({'name': 't2'}))
    t1.ensure('by_name', '{name}', duplicates=True)
    t2.ensure('by_name', '{name}', duplicates=True)
    have = set(db.tables(all=True))
    if version == 1:
        want = set(['__audit_table__', '__metadata__', '_people_by_age', '_people_by_name', '_test1_by_name', '_test2_by_name', 'people', 'test1', 'test2'])
        assert have == want
        db.drop('people')
        assert set(db.keys()) == set(['__audit_table__', '__metadata__', 'test1', 'test2'])
        have = set(db.tables(all=True))
        want = set(['__audit_table__', '__metadata__', '_test1_by_name', '_test2_by_name', 'test1', 'test2'])
        assert have == want
    else:
        want = set(['@@catalog@@', '@@audit_table@@', '@@metadata@@', 'people', 'test1', 'test2'])
        assert have == want
        db.drop('people')
        assert set(db.keys()) == set(['@@catalog@@', '@@audit_table@@', '@@metadata@@', 'test1', 'test2'])
        have = set(db.tables(all=True))
        want = set(['@@catalog@@', '@@audit_table@@', '@@metadata@@', 'test1', 'test2'])
        assert have == want


def test_index_attributes (people2, data2):
    people2.ensure('by_tags', 'def func(doc): return [tag.encode() for tag in doc["tags"]]', duplicates=True)
    assert people2['by_tags'].records() == 16
    have = [doc._name for doc in people2.seek('by_tags', Doc({'tags': ['yellow']}))]
    want = ['Tom', 'Harry', 'Sam']
    assert have == want
    assert [doc._name for doc in people2.seek('by_tags', Doc({'tags': ['purple']}))] == ['Sally']
    have = [doc._name for doc in people2.seek('by_tags', Doc({'tags': ['yellow', 'purple']}))]
    want = ['Tom', 'Harry', 'Sam', 'Sally']
    assert have == want

    people2.append(Doc({'name': 'Fred', 'tags': ['black']}))
    assert [doc._name for doc in people2.seek('by_tags', Doc({'tags': ['black']}))] == ['Fred']
    assert people2['by_tags'].records() == 17

    tom = people2.seek_one('by_name', Doc({'name': 'Tom'}))
    tom._tags = []
    people2.save(tom)

    assert people2['by_tags'].records() == 12
    assert [doc._name for doc in people2.seek('by_tags', Doc({'tags': ['yellow']}))] == ['Harry', 'Sam']

def test_ensure (people, data):
    people.ensure('by_name', '{name}', duplicates=False, force=True)
    with pytest.raises (DuplicateKey):
        for person in data:
            doc = Doc(person)
            people.append(doc)
            person['_id'] = doc.key

    assert people['by_name'].records() == 5
    assert people['by_age'].records() == 5
    people.ensure('by_name', '{age}', force=True, duplicates=True)
    for person in data:
        doc = Doc(person)
        people.append(doc)
        person['_id'] = doc.key

    assert people['by_name'].records() == 10
    assert people['by_age'].records() == 10

def test_range_none (people, data):
    docs = [doc for doc in people.range('by_name', Doc({'name': 'zz'}), Doc({'name': 'zz'}))]
    assert docs == []

def test_empty (people, data):
    assert people.records() == 5
    people.empty()
    assert people.records() == 0

    assert len(people) == 2
    assert list(people.keys()) == ['by_name', 'by_age']

    people.drop('by_name')

    assert len(people) == 1
    assert list(people.keys()) == ['by_age']

    people.drop('by_age')

    assert len(people) == 0
    assert list(people.keys()) == []

    people.ensure('by_name', '{name}', duplicates=True)
    people.ensure('by_age', '{age}')
    assert len(people) == 2
    assert list(people.keys()) == ['by_name', 'by_age']

    assert people['by_name'].records() == 0
    assert people['by_age'].records() == 0

def test_reindex (people, data):
    assert len(people) == 2
    assert list(people.keys()) == ['by_name', 'by_age']
    people.drop('by_name')
    assert len(people) == 1
    assert list(people.keys()) == ['by_age']
    people.drop('by_age')
    assert len(people) == 0
    assert list(people.keys()) == []
    people.ensure('by_name', '{name}', duplicates=True)
    people.ensure('by_age', '{age}')
    assert len(people) == 2
    assert list(people.keys()) == ['by_name', 'by_age']

    assert people['by_name'].records() == 5
    assert people['by_age'].records() == 5

    assert [p._name for p in people.find('by_name')] == ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
    assert [p._age for p in people.find('by_age')] == [20, 21, 25, 30, 35]

    with pytest.raises(NoSuchIndex):
        people.reindex('no index')

def test_index_change (people, data):
    doc = people.first()
    doc._age = 99
    people.save(doc)
    assert people['by_name'].records() == 5
    assert people['by_age'].records() == 5
    ages = [doc.key for doc in people.range('by_age', keyonly=True)]
    assert ages == [b'21', b'25', b'30', b'35', b'99']

def test_save_2 (people, data):
    for person in people.find():
        del person['name']
        people.save(person)

    assert people['by_name'].records() == 0
    assert people['by_age'].records() == 5

    with pytest.raises(DocumentDoesntExist):
        person.oid = None
        people.save(person)

def test_put (people, data):
    doc = Doc({'xxx': 'yyy'})
    people.append(doc)

def test_put_index_write_error (people, data):
    people.ensure('by_name', '{name}', duplicates=False, force=True)
    doc = Doc({'name': 'Fred'})
    people.append(doc)
    with pytest.raises(DuplicateKey):
        people.append(doc)

    doc.oid = None
    with pytest.raises(DuplicateKey):
        people.append(doc)

def test_add_dup_index (people, data):
    with pytest.raises(IndexAlreadyExists):
        people['by_name'] = Index(people._database, None, {'func': '     '})

def test_repr (people):
    assert str(people) == '<orbit_database.table.Table instance> name="people" status=open'

def test_drop_index (people):
    people.drop('by_name')
    assert len(people) == 1
    with pytest.raises(NoSuchIndex):
        people.drop('no index')

def test_range_no_index (people, data):
    lower = Doc(data[1])
    upper = Doc(data[0])
    with pytest.raises(NoSuchIndex):
        [doc._name for doc in people.range('by_xxx', lower=lower, upper=upper)]

def test_seek_one_no_index (people):
    with pytest.raises(NoSuchIndex):
        people.seek_one('by_xxx', Doc({'name': 'Tom'}))

def test_seek_no_index (people):
    with pytest.raises(NoSuchIndex):
        [person for person in people.seek('by_xxx', Doc({'name': 'Tom'}))]

def test_seek_keyonly (people, data):
    have = [cursor.val for cursor in people.seek('by_name', Doc({'name': 'Tom'}), keyonly=True)]
    want = [data[0]['_id']]
    assert have == want

def test_duplicate_keyonly (people, data):
    #
    #   Get all records with doc._name == "Dick"
    #
    load_people(people, data)
    names = [person._name for person in people.seek('by_name', Doc({'name': 'Dick'}))]
    assert names == ['Dick', 'Dick']
    #
    #   Get all records with doc._name == "Dick", but just the keys and limit the results to 1
    #
    result = list(people.seek('by_name', Doc({'name': 'Dick'}), keyonly=True, limit=1))[0]
    #
    #   Check the cursor.count for the single return is == 2
    #
    assert result.count == 2
    #
    #   Use the cursor result to fetch the actual data record
    #
    assert result.fetch().doc == {'name': 'Dick', 'age': 30}

def test_index_existing (people, data):
    people.ensure('by_name', '{name}', duplicates=True)
    people.ensure('by_age', '{age}', duplicates=True)
    assert people['by_name'].records() == 5
    assert people['by_age'].records() == 5

def test_multiple_keys_contrived(db):
    items = [
        {'name': 'Alpha', 'val': [1, 2, 3, 4, 9, 19]},
        {'name': 'Beta', 'val': [2, 3, 4]},
        {'name': 'Gamma', 'val': [3, 4, 8, 18]},
        {'name': 'Omega', 'val': [1, 2]},
        {'name': 'Epsilon', 'val': [1, 20]},
    ]
    data = db.table('data')
    data.ensure('by_name', '{name}', duplicates=True)
    data.ensure('by_val', 'def func(doc): return [f"{val:>3}".encode() for val in doc["val"]]', duplicates=True)

    for item in items:
        data.append(Doc(item))
    assert [doc._name for doc in data.find()] == ['Alpha', 'Beta', 'Gamma', 'Omega', 'Epsilon']
    assert [doc._name for doc in data.range('by_name')] == ['Alpha', 'Beta', 'Epsilon', 'Gamma', 'Omega']
    #
    #   So this is relatively obvious from above
    #
    oneor20 = ['Alpha', 'Omega', 'Epsilon', 'Epsilon']
    assert [doc._name for doc in data.seek('by_val', Doc({'val': [1, 20]}))] == oneor20
    #
    #   This is more complex, nodups, so there will be only 2 results as two keys are going in, one
    #   will represent the first with a '1' and the other, the first with a 20.
    #
    oneor20nodups = ['Alpha', 'Epsilon']
    lower = upper = Doc({'val': [1, 20]})
    assert [doc._name for doc in data.range('by_val', lower, upper, nodups=True)] == oneor20nodups
