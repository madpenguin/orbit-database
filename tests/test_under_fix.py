# from orbit_database import Manager, Doc
# from shutil import rmtree
# from pathlib import Path
# import pytest

# DATABASE = '.test_database'


# @pytest.fixture
# def db():
#     rmtree(DATABASE, ignore_errors=True)
#     db = Manager().database(DATABASE)
#     yield db
#     db.close()

# @pytest.fixture
# def data():
#     return [
#         {'name': 'Tom', 'age': 20},
#         {'name': 'Dick', 'age': 30},
#         {'name': 'Harry', 'age': 25},
#         {'name': 'Sam', 'age': 35},
#         {'name': 'Sally', 'age': 21},
#     ]


# def test_database_name(db):
#     assert db._path == DATABASE, 'database path is wrong'
#     assert db._name == DATABASE.replace('_', '-'), 'database name is wrong'


# def test_table_name(db):
#     name = 'this_is_bad'
#     table = db.table(name)
#     assert table.name == name.replace('_', '-')