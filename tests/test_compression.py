from orbit_database import Manager, Doc, CompressionType, WriteTransaction
from shutil import rmtree
import os
from ujson import dumps
import pytest
from time import sleep

DATABASE = '.test_database'
COMPRESSIONS = [(CompressionType.ZSTD, 5), (CompressionType.SNAPPY, None), (CompressionType.NONE, None)]


@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]

@pytest.fixture(params=[1,2])
def version (request):
    return request.param


@pytest.fixture
def db(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db
    sleep(2)
    db.close()


@pytest.fixture(params=COMPRESSIONS)
def people(request, data, db):
    compression_type = request.param[0]
    compression_level = request.param[1]
    result = db['people'].open(compression_type=compression_type, compression_level=compression_level, compress_existing=True)
    with WriteTransaction(db) as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result


def test_zstd_training_from_sample_records(data, db):
    people = db['people'].open()
    with WriteTransaction(db) as txn:
        for i in range(10):
            [person.update({'_id': people.append(Doc(person), txn=txn).key}) for person in data]
    assert people.zstd_train(training_record_count=people.records()) is None, (
        'Failed to acquire training data')


# def test_zstd_training_from_samples(data, db):
#     people = db['people'].open()
#     with WriteTransaction(db) as txn:
#         for i in range(10):
#             [person.update({'_id': people.append(Doc(person), txn=txn).key}) for person in data]

#     samples = []
#     for i in range(10):
#         for sample in data:
#             samples.append(dumps(sample).encode())
#     assert people.zstd_train(training_samples=samples) is None, (
#         'Failed to acquire training data')


# def test_zstd_training_conflicting_params(data, db):
#     with pytest.raises(ValueError):
#         people = db['people'].open()
#         people.zstd_train(training_samples=['1'], training_record_count=1)


# def test_compress_retrain_more(data, db):
#     people = db.table('people')
#     for i in range(5000):
#         with WriteTransaction(db) as txn:
#             for person in data:
#                 doc = Doc(person)
#                 people.append(doc, txn=txn)
#                 person['_id'] = doc.key
#     people.zstd_train(10)
#     people.close()

#     people.open(CompressionType.ZSTD, 22, compress_existing=True)
#     assert people.compressed(), True
#     assert people.compression_type() == CompressionType.ZSTD

#     for doc in people.find(limit=1):
#         assert doc.doc == {'name': 'Tom', 'age': 20}

#     people.close()
#     people.open()
#     assert people.compressed() == True
#     assert people.compression_type() == CompressionType.ZSTD
#     for doc in people.find(limit=1):
#         assert doc.doc == {'name': 'Tom', 'age': 20}

# def test_compress_retrain_less(data, db):
#     people = db.table('people')
#     for i in range(5000):
#         with WriteTransaction(db) as txn:
#             for person in data:
#                 doc = Doc(person)
#                 people.append(doc, txn=txn)
#                 person['_id'] = doc.key
#     people.zstd_train(10)
#     people.close()

#     people.open(compression_type=CompressionType.ZSTD, compression_level=3, compress_existing=True)
#     assert people.compressed()
#     assert people.compression_type() ==  CompressionType.ZSTD
#     for doc in people.find(limit=1):
#         assert doc.doc == {'name': 'Tom', 'age': 20}
#     with pytest.raises(TrainingDataExists):
#         people.zstd_train(0)
#     people.close()

#     people = db.table('people')
#     assert people.compressed() == True
#     assert people.compression_type() == CompressionType.ZSTD
#     for doc in people.find(limit=1):
#         assert doc.doc == {'name': 'Tom', 'age': 20}

#     people.empty()
#     for i in range(100):
#         with WriteTransaction(db) as txn:
#             for person in data:
#                 doc = Doc(person)
#                 people.append(doc, txn=txn)
#                 person['_id'] = doc.key

#     for doc in people.find(limit=1):
#         assert doc._name == 'Tom'
#         assert doc._age == 20


# def test_write_then_compress(data, db):
#     people = db.table('people')
#     for i in range(5):
#         with WriteTransaction(db) as txn:
#             for person in data:
#                 doc = Doc(person)
#                 people.append(doc, txn=txn)
#                 person['_id'] = doc.key

#     people.zstd_train(10)
#     people.close()

#     people.open(CompressionType.ZSTD, 22, compress_existing=True)
#     assert people.compressed(), True
#     assert people.compression_type() == CompressionType.ZSTD

#     for doc in people.find(limit=1):
#         assert doc.doc == {'name': 'Tom', 'age': 20}


# class UnitTests(TestCase):

#     DATABASE = '.test_database'

#     def setUp(self):
#         rmtree(self.DATABASE, ignore_errors=True)
#         self.data = [
#             {'name': 'Tom', 'age': 20},
#             {'name': 'Dick', 'age': 30},
#             {'name': 'Harry', 'age': 25},
#             {'name': 'Sam', 'age': 35},
#             {'name': 'Sally', 'age': 21},
#         ]
#         self.cdata = [
#             {'name': 'key1', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
#             {'name': 'key2', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
#             {'name': 'key3', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
#         ]

#     def tearDown(self):
#         pass

#     def setup_people(self):
#         manager = Manager()
#         manager['mydb'].open(self.DATABASE)
#         people = manager['mydb']['people'].open()
#         self.load(people)
#         self.manager = manager
#         return people

#     def load(self, people):
#         with WriteTransaction(people._database) as txn:
#             for person in self.data:
#                 doc = Doc(person)
#                 people.append(doc, txn=txn)
#                 person['_id'] = doc.key

#     def test_open_compression(self):
#         people = Manager().database(self.DATABASE)['people'].open()
#         self.assertFalse(people.compressed())

#     def test_compress_unknown(self):
#         with self.assertRaises(Exception):
#             Manager().database(self.DATABASE)['people'].open(compression_type='z')

#     def test_compress_unknown_2(self):
#         people = Manager().database(self.DATABASE)['people'].open()
#         self.assertEqual(people.compression_type(), CompressionType.NONE)

#     def test_compress_zstd(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.ZSTD, 5)
#         self.assertTrue(people.compressed())
#         self.assertEqual(people.compression_type(), CompressionType.ZSTD)

#     def test_compress_snappy(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.SNAPPY, 5)
#         self.assertTrue(people.compressed())
#         self.assertEqual(people.compression_type(), CompressionType.SNAPPY)

#     def test_compress_zstd_data_notopen(self):
#         people = Manager().database(self.DATABASE)['people']
#         with self.assertRaises(TableNotOpen):
#             people.zstd_train(people.records())

#     def test_compress_zstd_data(self):
#         people = Manager().database(self.DATABASE)['people'].open()
#         for i in range(20):
#             self.load(people)
#         people.zstd_train(people.records() + 1)
#         people.close()

#         people.open(CompressionType.ZSTD, 22)
#         self.assertTrue(people.compressed())
#         self.assertEqual(people.compression_type(), CompressionType.ZSTD)

#         for i in range(20000):
#             self.load(people)
#         st = os.stat(f'{self.DATABASE}/data.mdb')
#         size1 = st.st_blocks * 512 / 1024 / 1024

#         rmtree(self.DATABASE, ignore_errors=True)

#         people = Manager().database(self.DATABASE)['people']
#         people.open()
#         for i in range(20):
#             self.load(people)

#         self.assertFalse(people.compressed())
#         self.assertEqual(people.compression_type(), CompressionType.NONE)

#         for i in range(20000):
#             self.load(people)
#         st = os.stat(f'{self.DATABASE}/data.mdb')
#         size2 = st.st_blocks * 512 / 1024 / 1024

#         self.assertGreater(size2 - size1, 2)

#     def test_compress_snappy_data(self):
#         db = Manager().database(self.DATABASE)
#         data = db['people'].open(CompressionType.SNAPPY)

#         self.assertTrue(data.compressed())
#         self.assertEqual(data.compression_type(), CompressionType.SNAPPY)

#         for i in range(20000):
#             with WriteTransaction(db) as txn:
#                 for d in self.cdata:
#                     doc = Doc(d)
#                     data.append(doc, txn=txn)
#                     d['_id'] = doc.key

#         st = os.stat(f'{self.DATABASE}/data.mdb')
#         size1 = st.st_blocks * 512 / 1024 / 1024

#         rmtree(self.DATABASE, ignore_errors=True)

#         # db = Manager()['mydb'].open(self.DATABASE)
#         # data = db['people'].open()
#         db = Manager().database(self.DATABASE, config={'map_size':1024*1024*1024*2})
#         data = db.table('people')
#         for i in range(20000):
#             with WriteTransaction(db) as txn:
#                 for d in self.cdata:
#                     doc = Doc(d)
#                     data.append(doc, txn=txn)
#                     d['_id'] = doc.key

#         st = os.stat(f'{self.DATABASE}/data.mdb')
#         size2 = st.st_blocks * 512 / 1024 / 1024

#         self.assertGreater(size2 - size1, 3)

#     def test_compress_test_reopen(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.ZSTD, 3)
#         for i in range(5000):
#             self.load(people)
#         people.open()

#     def test_compress_test_open_unknown(self):
#         db = Manager().database(self.DATABASE)
#         people = db['people'].open(CompressionType.ZSTD, 3)
#         for i in range(1):
#             self.load(people)
#         people.close()
#         people = db['people'].open(CompressionType.ZSTD, 3)

#     def test_compress_test_bad_training(self):
#         db = Manager().database(self.DATABASE)
#         people = db['people'].open()
#         for i in range(1):
#             self.load(people)
#         result = people.zstd_train(0)
#         self.assertEqual(str(result), 'cannot train dict: Src size is incorrect')

#     def test_compress_drop(self):
#         db = Manager().database(self.DATABASE)
#         people = db['people'].open()
#         for i in range(100):
#             self.load(people)
#         people.zstd_train(10)
#         people.close()
#         people.open(CompressionType.ZSTD, 22)

#         self.assertEqual([key[0] for key in people._meta.fetch(people.name)], ['_people!zstd_cdict', '_people@config'])
#         db.drop('people')
#         self.assertEqual([key[0] for key in people._meta.fetch(people.name)], [])

#     def test_compress_drop_with_index(self):
#         db = Manager().database(self.DATABASE)
#         people = db['people'].open()
#         people.ensure('by_name', '{name}', duplicates=True)
#         for i in range(100):
#             self.load(people)
#         people.zstd_train(10)
#         people.close()
#         people.open(CompressionType.ZSTD, 22)


#     def test_range_all_incl2(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
#         people.ensure('by_name', '{name}', duplicates=True)
#         self.load(people)
#         lower = Doc(self.data[1])
#         upper = Doc(self.data[0])
#         self.assertEqual(
#             [doc._name for doc in people.range('by_name', lower=lower, upper=upper)],
#             ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
#         )

#     def test_seek_one(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
#         people.ensure('by_name', '{name}', duplicates=True)
#         self.load(people)
#         del self.data[0]['_id']
#         self.assertEqual(people.seek_one('by_name', Doc({'name': 'Tom'})).doc, self.data[0])

#     def test_seek_name(self):
#         people = Manager().database(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
#         people.ensure('by_name', '{name}', duplicates=True)
#         self.load(people)
#         del self.data[0]['_id']
#         del self.data[3]['_id']
#         self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))], [self.data[0]])
#         self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))], [self.data[3]])

