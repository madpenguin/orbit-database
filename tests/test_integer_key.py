#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from orbit_database import Manager, Doc
from shutil import rmtree
from time import sleep
import pytest

DATABASE = '.test_database'

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def database(version):
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'version': version})
    yield db


def test_create_table(database):
    people = database.table('people', integerkey=True)
    people.append(Doc({'One': 1}))
    people.append(Doc({'Two': 2}))
    people.append(Doc({'Three': 3}))
    assert [result.doc.key for result in people.filter()] == [0, 1, 2]


def test_get_key(database):
    people = database.table('people', integerkey=True)
    people.append(Doc({'One': 1}))
    doc = people.get(0)
    assert doc.doc == {'One': 1}

def test_del_key(database):
    people = database.table('people', integerkey=True)
    people.append(Doc({'One': 1}))
    assert people.records() == 1
    people.delete(0)
    assert people.records() == 0
