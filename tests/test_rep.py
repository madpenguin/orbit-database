#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
# from pynndb import Manager, Database, ReadTransaction, Doc, WriteTransaction

# DATABASE = '.database'

# def test_database_name(tmp_path):

#     db = Manager().database('db', f'{tmp_path}/{DATABASE}')
#     db.configure({'map_size': 1024 * 100})
#     db.open(DATABASE)

#     assert db.name == db.replication.uuid, 'name mismatch'


# def test_database_rep(tmp_path):

#     db = Manager().database('db', f'{tmp_path}/{DATABASE}')
#     db.configure({'map_size': 1024 * 100, 'replication': True})
#     db.open(DATABASE)


# def test_database_read_transaction(tmp_path):

#     db = Manager().database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     table = db.table('people')
#     table.append(Doc({'name': 'fred'}))
#     with ReadTransaction(db) as txn:
#         for result in table.filter(txn=txn):
#             assert result.raw == b'{"name":"fred"}', 'raw record failed'

# def test_database_reopen(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     xy = manager.database(f'{tmp_path}/{DATABASE}')
#     assert isinstance(xy, Database), 'db reopen failed'
#     db.close()


# def test_metadata(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     with WriteTransaction(db) as txn:
#         db.meta.store_int('mykey', 10, txn=txn)
#         val = db.meta.fetch_int('mykey', txn=txn)
#         assert val == 10, 'store/fetch meta failed'
#         db.meta.remove_key('mykey', txn=txn)
#         val = db.meta.fetch_int('mykey', txn=txn)
#         assert val is None, 'remove meta failed'

# def test_database_drop_journaled(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     db.table('todelete')
#     db.drop('todelete')


# def test_database_indexes(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     table = db.table('todelete')
#     table.ensure('index1', '{name}')
#     table.ensure('index2', '{age}')
#     assert list(table.indexes()) == ['index1', 'index2'], 'index name list is wrong'


# def test_database_save_del(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     table = db.table('todelete')
#     doc = Doc({'name': 'fred'})
#     table.append(doc)
#     doc._age = 24
#     table.save(doc)
#     table.delete(doc.key)

# def test_database_empty(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     table = db.table('todelete')
#     doc = Doc({'name': 'fred'})
#     table.append(doc)
#     doc._age = 24
#     table.save(doc)
#     table.empty()


# def test_database_drop_index(tmp_path):

#     manager = Manager()
#     db = manager.database(f'{tmp_path}/{DATABASE}', config={'map_size': 1024 * 100, 'replication': True})
#     table = db.table('todelete')
#     table.ensure('by_name', '{name}')
#     doc = Doc({'name': 'fred'})
#     table.append(doc)
#     table.drop('by_name')
#     assert len(list(table.indexes())) == 0, 'drop index failed'
