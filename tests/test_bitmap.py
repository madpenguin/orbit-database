import pytest
from orbit_database import Manager, WriteTransaction, Bitmap, InvertedWordIndex
from tempfile import TemporaryDirectory
from pathlib import Path

DATABASE = '.test_database'

@pytest.fixture(params=[1,2])
def version (request):
    return request.param

@pytest.fixture
def database(version):
    with TemporaryDirectory() as dir:
        db_file_name = (Path(dir) / DATABASE).as_posix()
        db = Manager().database('db', db_file_name, config={'version': version})
        table = db.table('test')
        table.ensure('full_text', iwx=True)
        yield db


def test_write_entry(database):
    with WriteTransaction(database) as transaction:
        table = database.env.open_db(key='bitmap_test'.encode(), txn=transaction.txn)
        bitmap = Bitmap(table)
        bitmap.update(0,0,True, transaction)
        page = bitmap.fetch(0,0, transaction)
        test = b'\x01' + (b'\x00' * (InvertedWordIndex.BITMAP_CHUNK-1))
        assert page == test, 'bitmap wrong'


def test_write_multiple(database):
    with WriteTransaction(database) as transaction:
        table = database.env.open_db(key='bitmap_test'.encode(), txn=transaction.txn)
        bitmap = Bitmap(table)
        bitmap.update(0,0,True, transaction)
        bitmap.update(0,1,True, transaction)
        bitmap.update(0,2,True, transaction)
        page = bitmap.fetch(0,0, transaction)
        test = b'\x07' + (b'\x00' * (InvertedWordIndex.BITMAP_CHUNK-1))
        assert page == test, 'bitmap wrong'


def test_delete_multiple(database):
    with WriteTransaction(database) as transaction:
        table = database.env.open_db(key='bitmap_test'.encode(), txn=transaction.txn)
        bitmap = Bitmap(table)
        bitmap.update(0,0,True, transaction)
        bitmap.update(0,1,True, transaction)
        bitmap.update(0,2,True, transaction)
        bitmap.update(0,0,False, transaction)
        bitmap.update(0,2,False, transaction)
        page = bitmap.fetch(0,0, transaction)
        test = b'\x02' + (b'\x00' * (InvertedWordIndex.BITMAP_CHUNK-1))
        assert page == test, 'bitmap wrong'


def test_big(database):
    with WriteTransaction(database) as transaction:
        table = database.env.open_db(key='bitmap_test'.encode(), txn=transaction.txn)
        bitmap = Bitmap(table)
        bitmap.update(1,0,True, transaction)
        bitmap.update(1,1,True, transaction)
        bitmap.update(1,8,True, transaction)
        bitmap.update(1,17,True, transaction)
        bitmap.update(2,0,True, transaction)
        bitmap.update(2,1,True, transaction)
        bitmap.update(2,8,True, transaction)
        bitmap.update(2,17,True, transaction)

        bitmap.update(1,0,False, transaction)
        bitmap.update(1,1,False, transaction)
        bitmap.update(1,8,False, transaction)
        bitmap.update(1,17,False, transaction)
        bitmap.update(2,0,False, transaction)
        bitmap.update(2,1,False, transaction)
        bitmap.update(2,8,False, transaction)
        bitmap.update(2,17,False, transaction)

        # page = bitmap.fetch(0,0,True, transaction.txn)
        # print(page)

        test = b'\x00' * (InvertedWordIndex.BITMAP_CHUNK)
        page = bitmap.fetch(1,1, transaction)
        assert page == test, 'bitmap wrong'
        page = bitmap.fetch(2,1, transaction)
        assert page == test, 'bitmap wrong'
